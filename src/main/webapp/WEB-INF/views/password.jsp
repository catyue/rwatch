<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath(); //web项目的根路径
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<title>健康数据</title>
<link rel="stylesheet" href="<%=basePath%>static/layui2.4.5/css/layui.css">
<link rel="stylesheet" href="<%=basePath%>static/font-awesome/css/font-awesome.min.css">
<script src="<%=basePath%>static/jquery/jquery.min.js"></script>
<script src="<%=basePath%>static/layui2.4.5/layui.js"></script>
<script src="<%=basePath%>static/layui2.4.5/lay/modules/layer.js"></script>
</head>
<body>
	<form class="layui-form" action="">
		<div class="layui-form-item">
			<label class="layui-form-label">旧密码</label>
			<div class="layui-input-inline">
				<input type="text" id="oldPassword" name="oldPassword" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" onblur="compareWithPassword()">
  			</div>
  		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">新密码</label>
			<div class="layui-input-inline">
				<input type="text" id="newPassword" name="newPassword" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" onblur="compareWithOld()">
  			</div>
  		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">确认新密码</label>
			<div class="layui-input-inline">
				<input type="text" id="checkPassword" name="checkPassword" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" onblur="compareWithNew()">
  			</div>
  		</div>
  		<div class="layui-form-item">
    		<div class="layui-input-block">
      			<button class="layui-btn" lay-submit="" lay-filter="go">确认修改</button>
    		</div>
  		</div>
	</form>	    	
	<script type="text/javascript">
		function compareWithPassword() {
			var oldPassword = $('#oldPassword').val()
			$.ajax({  
				type : "post",  //提交方式  
				url : "<%=basePath %>account/checkPassword",//路径  
				data : {  
				   "oldPassword": oldPassword
				},//数据，必须为 Key/Value 格式  
				success : function(result) { //返回数据根据结果进行相应的处理 
					if(result.success){						
						layer.tips("旧密码错误！", '#oldPassword')
					}
				}
			});
		}
		
		function compareWithOld() {
			var oldPassword = $('#oldPassword').val()
			var newPassword = $('#newPassword').val()
			if(newPassword == "") {
				layer.tips("新密码不能为空！", '#newPassword')
			} else if(oldPassword == newPassword) {
				layer.tips("不能与旧密码相同！", '#newPassword')
				$('#newPassword').val('')
			}
		}
		
		function compareWithNew() {
			var newPassword = $('#newPassword').val()
			var checkPassword = $('#checkPassword').val()
			if(newPassword != checkPassword) {
				layer.tips("与新密码不一致！", '#checkPassword')
				$('#checkPassword').val('')
			}
		}
		
		layui.use('form', function(){
			  var form = layui.form;
			//监听提交
			  form.on('submit(go)', function(){
				  var newPassword = $('#newPassword').val()
				  $.ajax({  
						type : "post",  //提交方式  
						url : "<%=basePath %>account/editPassword",//路径  
						data : {  
						   "newPassword": newPassword
						},//数据，必须为 Key/Value 格式  
						success : function(result) { //返回数据根据结果进行相应的处理 
							if(result.success){						
								layer.msg("修改成功！")
							}
						},
					 	error: function(result) {  //返回数据根据结果进行相应的处理 
					 		layer.msg("修改出错！")
						}
					});
			    	return false;
			  });
			});
	</script>
</body>
</html>