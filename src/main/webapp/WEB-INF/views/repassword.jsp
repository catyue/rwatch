<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath(); //web项目的根路径
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!doctype html>
<html lang="zh">
<head>
	<meta charset="UTF-8">
	<title>管理员找密码-R-Watch</title>
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link rel="stylesheet" href="<%=basePath%>static/css/font.css">
	<link rel="stylesheet" href="<%=basePath%>static/css/rwatch.css">
	<script src="<%=basePath %>static/jquery/jquery.min.js"></script>
	<script src="<%=basePath%>static/layui2.4.5/layui.js"></script>
<style type="text/css">
.layui-input-inline{
    line-height:34px;
}
</style>
</head>
<body class="login-bg">

 <div class="login">
        <div class="message">R-Watch智能手环找回密码</div>
        <div id="darkbannerwrap"></div>
        
       <form class="layui-form">
            <!-- 密码 -->
        <div class="layui-input-inline">
            <div class="layui-inline" style="width: 85%">
                密&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;码:<input type="password" id="possword" name="password" required  lay-verify="required" placeholder="请输入密码" autocomplete="off" class="layui-input">
            </div>
            <!-- 对号 -->
            <div class="layui-inline" style="padding-top: 34px;">
                <i class="layui-icon" id="pri" style="color: green;font-weight: bolder;" hidden></i>
            </div>
            <!-- 错号 -->
            <div class="layui-inline" style="padding-top: 34px;">
                <i class="layui-icon" id="pwr" style="color: red; font-weight: bolder;" hidden>ဆ</i>
            </div>
        </div>
            <!-- 确认密码 -->
        <div class="layui-input-inline">
            <div class="layui-inline" style="width: 85%">
                确认密码:<input type="password" id="repossword" name="repassword" required  lay-verify="required" placeholder="请确认密码" autocomplete="off" class="layui-input">
            </div>
            <!-- 对号 -->
            <div class="layui-inline" style="padding-top: 34px;">
                <i class="layui-icon" id="rpri" style="color: green;font-weight: bolder;" hidden></i>
            </div>
            <!-- 错号 -->
            <div class="layui-inline" style="padding-top: 34px;">
                <i class="layui-icon" id="repossword" style="color: red; font-weight: bolder;" hidden>ဆ</i>
            </div>
        </div>
        <div class="layui-input-inline login-btn" style="line-height: 72px;width: 100%">
            <button type="submit" lay-submit lay-filter="sub" class="layui-btn" style="width:100%">找回密码</button>
        </div>
        <hr style="width: 85%" />
        <p style="width: 85%"><a href="<%=basePath %>login/index" class="fl">已有账号？立即登录</a></p>
    </form>
    </div>

<script src="<%=basePath %>static/layui2.4.5/layui.js"></script>
<script type="text/javascript">
    layui.use(['form','jquery','layer'], function () {
        var form   = layui.form;
        var $      = layui.jquery;
        var layer  = layui.layer;
        // you code ...
        // 为密码添加正则验证
        $('#possword').blur(function() {
                var reg = /^[\w]{6,12}$/;
                if(!($('#possword').val().match(reg))){
                    //layer.msg('请输入合法密码');
                    $('#pwr').removeAttr('hidden');
                    $('#pri').attr('hidden','hidden');
                    layer.msg('请输入合法密码');
                }else {
                    $('#pri').removeAttr('hidden');
                    $('#pwr').attr('hidden','hidden');
                }
        });
        //验证两次密码是否一致
        $('#repossword').blur(function() {
                if($('#possword').val() != $('#repossword').val()){
                    $('#repossword').removeAttr('hidden');
                    $('#rpri').attr('hidden','hidden');
                    layer.msg('两次输入密码不一致!');
                }else {
                    $('#rpri').removeAttr('hidden');
                    $('#repossword').attr('hidden','hidden');
                };
        });
 
        //
        //添加表单监听事件,提交注册信息
        form.on('submit(sub)', function() {
        	//var userName = $('#userName').val();
            $.ajax({
                url:'<%= basePath %>login/uppPassword',
                type:'get',
                dataType:'json',
                data:{
                    "password":$('#possword').val(),
                    "repassword":$('#repossword').val(),
                },
                success:function(data){
                    if (data.success) {
                          location.href = "<%= basePath %>login/index";
                    }else {
                        location.href = "<%= basePath %>login/updataPassword";
                    }
                }
            })
            //防止页面跳转
            return false;
        });
 
    });
</script>
</body>
</html>