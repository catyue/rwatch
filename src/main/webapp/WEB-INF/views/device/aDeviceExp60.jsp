<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath(); //web项目的根路径
			String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
					+ path + "/";
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<title>设备管理</title>
<link rel="stylesheet" href="<%=basePath%>static/layui2.4.5/css/layui.css">
<link rel="stylesheet" href="<%=basePath%>static/font-awesome/css/font-awesome.min.css">
<script src="<%=basePath%>static/jquery/jquery.min.js"></script>
<script src="<%=basePath%>static/layui2.4.5/layui.js"></script>
</head>
<body>

<div class="layui-inline">
   <label class="layui-form-label">IMEI号</label>
   <div class="layui-input-inline">
     <input type="text" id="deviceName" name="deviceName" placeholder="请输入" autocomplete="off" class="layui-input">
   </div>
   <a class="layui-btn search_btn "  id="searchBtn"  data-type="getInfo" style="margin-left: 15px;">查询</a>
</div>


	<table class="layui-hide" id="device"></table>
	<script type="text/html" id="bar">
  		<a class="layui-btn layui-btn-xs" lay-event="edit">修改信息</a>
  		<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="find">查看历史</a>
	</script>
	<script>
		layui.use('table', function() {
			
			$('#searchBtn').on('click',function(){
		        var type = $(this).data('type');
		        active[type] ? active[type].call(this) : '';
		    });
		    // 点击获取数据
		    var  active = {
		 getInfo: function () {
		            var deviceName=$('#deviceName').val();
		            // var startTime=$('#startTime').val();
		            // var endTime=$('#endTime').val();
		            var timeRange=$('#timeRange').val();
			        if (deviceName!=null&&deviceName!="") {
		                var index = layer.msg('查询中，请稍候...',{icon: 16,time:false,shade:0});
		                setTimeout(function(){
		                    table.reload('device',
		                    		{
				                	url: '<%=basePath %>device/aDeviceExpData?time=60',
		                       		where: {
			                            'deviceName':deviceName,
			                            'timeRange':timeRange
		                        }
		                    });
		                    layer.close(index);
		                },800);
		            } else {
		            	table.reload(device);
		            }
		        }
		};
			
			
			
			
			
			
			var table = layui.table;
			 table.render({
				    elem: '#device'
				    ,url:'<%=basePath %>device/aDeviceExpData?time=60'//数据接口
				    ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
				    ,cols: [[
								{field:'deviceName', width:'25%', title: '设备名',sort: true,align: 'center'}
							   ,{field:'imei', width:'25%', title: 'IMEI号',sort: true,align: 'center'}
							   ,{field:'startTime', width:'25%', title: '激活时间', sort: true ,align: 'center'}
							   ,{field:'endTime', width:'25%', title: '用户到期',align: 'center'}
							  /* ,{field:'deviceId', width:'15%', title: '设备ID', sort: true ,align: 'center'}
						      ,{field:'memberId', width:'10%', title: '会员号' ,align: 'center'}
						      ,{field:'bymemberid',  width:'10%', title: '会员号2', sort: true ,align: 'center'}
						      ,{field:'userId', width:'15%', title: '用户id', sort: true ,align: 'center'}
						      ,{field:'delFlag', width:'15%', title: '状态',align: 'center'}
						      ,{field:'createBy', width:'15%', title: '创建createBY',sort: true,align: 'center'}
						      ,{field:'createDate', width:'15%', title: 'createDate', sort: true ,align: 'center'}
						      ,{field:'updateBy', width:'15%', title: 'updateBy',align: 'center'}
						      ,{field:'updateDate', width:'15%', title: 'updateDate',align: 'center'} */
						      ]]
			 		,title: '用户表'
				   ,page: true//开启分页
				   ,limit:5
				    ,limits:[5,10,15,20]
				    ,toolbar: 'default' //开启工具栏，此处显示默认图标，可以自定义模板，详见文档
				    ,totalRow: true //开启合计行
				  });
				});
	</script>
</body>
</html>
