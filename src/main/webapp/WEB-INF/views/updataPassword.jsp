<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath(); //web项目的根路径
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!doctype html>
<html lang="zh">
<head>
	<meta charset="UTF-8">
	<title>管理员忘记密码s-R-Watch</title>
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link rel="stylesheet" href="<%=basePath%>static/css/font.css">
	<link rel="stylesheet" href="<%=basePath%>static/css/rwatch.css">
	<script src="<%=basePath %>static/jquery/jquery.min.js"></script>
	<script src="<%=basePath%>static/layui2.4.5/layui.js"></script>
<style type="text/css">
.layui-input-inline{
    line-height:34px;
}
</style>
</head>
<body class="login-bg">

 <div class="login">
        <div class="message">R-Watch智能手环注册</div>
        <div id="darkbannerwrap"></div>
        
       <form class="layui-form">
       <!-- 用户名 -->
        <div class="layui-input-inline">
            <div class="layui-inline" style="width: 85%">
              	  用&nbsp;&nbsp;户&nbsp;&nbsp;名:<input type="text" id="userName" name="userName" required  lay-verify="required" placeholder="请输入用户名" autocomplete="off" class="layui-input">
       		</div>
       	</div>
       		<!-- 密码 -->
       	<div class="layui-input-inline">
            <div class="layui-inline" style="width: 85%">
              	  手&nbsp;&nbsp;机&nbsp;&nbsp;号:<input type="text" id="phone" name="phone" required  lay-verify="required" placeholder="请输入手机号" autocomplete="off" class="layui-input">
       		</div>
       	</div>
	        <div class="layui-input-inline login-btn" style="line-height: 72px;width: 100%">
	            <button type="submit" lay-submit lay-filter="sub" class="layui-btn" style="width:100%">找回密码</button>
	        </div>
    </form>
    </div>

<script src="<%=basePath %>static/layui2.4.5/layui.js"></script>
<script type="text/javascript">
    layui.use(['form','jquery','layer'], function () {
        var form   = layui.form;
        var $      = layui.jquery;
        var layer  = layui.layer;
        
        //添加表单监听事件,提交注册信息
        form.on('submit(sub)', function() {
        	//var userName = $('#userName').val();
            $.ajax({
                url:'<%= basePath %>login/findUpdataPassword',
                type:'post',
                dataType:'json',
                data:{
                   "userName":$('#userName').val(),
                    "phone":$('#phone').val(),
                },
                success:function(data){
                    if (data.success) {
                    	  layer.msg('注册成功');
                          location.href = "<%= basePath %>login/repassword";
                    }else {
                        layer.msg('注册失败');
                        location.href = "<%= basePath %>login/updataPassword";
                    }
                }
            })
            //防止页面跳转
            return false;
        });
 
    });
</script>
</body>
</html>