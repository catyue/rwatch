<%@page import="com.hsd.model.RUser"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath(); //web项目的根路径
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<title>健康数据</title>
<link rel="stylesheet" href="<%=basePath%>static/layui2.4.5/css/layui.css">
<link rel="stylesheet" href="<%=basePath%>static/font-awesome/css/font-awesome.min.css">
<script src="<%=basePath%>static/jquery/jquery.min.js"></script>
<script src="<%=basePath%>static/layui2.4.5/layui.js"></script>
<script src="<%=basePath%>static/layui2.4.5/lay/modules/layer.js"></script>
</head>
<body>
	<form class="layui-form" action="">
		<div class="layui-form-item">
			<label class="layui-form-label">客户名称</label>
			<div class="layui-input-inline">
				<input type="text" value="<%= ((RUser)session.getAttribute("user")).getUserId() %>" id="userName" name="userName" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" readonly="readonly">
  			</div>
  		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">登录账户</label>
			<div class="layui-input-inline">
				<input type="text" id="nickName" name="nickName" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" readonly="readonly">
  			</div>
  		</div>
<!-- 		<div class="layui-form-item">
			<label class="layui-form-label">联系人</label>
			<div class="layui-input-inline">
				<input type="text" id="checkPassword" name="checkPassword" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
  			</div>
  		</div> -->
		<div class="layui-form-item">
			<label class="layui-form-label">电话</label>
			<div class="layui-input-inline">
				<input type="text" id="phone" name="phone" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input" >
  			</div>
  		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">联系邮箱</label>
			<div class="layui-input-inline">
				<input type="text" id="email" name="email" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
  			</div>
  		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">地点</label>
			<div class="layui-input-inline">
				<input type="text" id="address" name="address" lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
  			</div>
  		</div>
		<div class="layui-form-item" pane="">
			<label class="layui-form-label">报警附加通知方式</label>
			<div class="layui-input-block">
				<input type="checkbox" name="alarm" lay-skin="primary" title="邮件通知">
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">离线报警开关</label>
			<div class="layui-input-block">
				<input type="radio" name="sw0" value="开" title="开">
				<input type="radio" name="sw0" value="关" title="关" checked="">
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">接受下级离线报警</label>
			<div class="layui-input-block">
				<input type="radio" name="sw1" value="开" title="开">
				<input type="radio" name="sw1" value="关" title="关" checked="">
			</div>
		</div>
		<div class="layui-form-item" pane="">
			<label class="layui-form-label">离线报警附加通知方式</label>
			<div class="layui-input-block">
				<input type="checkbox" name="outAlarm" lay-skin="primary" title="邮件通知">
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">ACC信息推送</label>
			<div class="layui-input-block">
				<input type="radio" name="sw2" value="开" title="开">
				<input type="radio" name="sw2" value="关" title="关" checked="">
			</div>
		</div>
		<div class="layui-form-item">
    		<div class="layui-input-block">
      			<button class="layui-btn" lay-submit="" lay-filter="go">确认修改</button>
    		</div>
  		</div>
	</form>
	
	<script type="text/javascript">
	
		layui.use('element', function(){
			  var element = layui.element; 
			});
		
		$(function(){
			var userName = '<%= ((RUser)session.getAttribute("user")).getUserName() %>';
			var nickName = '<%= ((RUser)session.getAttribute("user")).getNickName() %>';
			var phone = '<%= ((RUser)session.getAttribute("user")).getPhone() %>';
			var email = '<%= ((RUser)session.getAttribute("user")).getEmail() %>';
			var address = '<%= ((RUser)session.getAttribute("user")).getAddress() %>';
			$('#userName').val(userName)
			$('#nickName').val(nickName)
			$('#phone').val(phone)
			$('#email').val(email)
			$('#address').val(address)
		});
		
		layui.use('form', function(){
			  var form = layui.form;
			//监听提交
			  form.on('submit(go)', function(){
				  var phone = $('#phone').val()
				  var email = $('#email').val()
				  var address = $('#address').val()
				  $.ajax({  
						type : "post",  //提交方式  
						url : "<%=basePath %>account/editInformation",//路径  
						data : {
						   "phone": phone,
						   "email": email,
						   "address": address
						},//数据，必须为 Key/Value 格式  
						success : function(result) { //返回数据根据结果进行相应的处理 
							if(result.success){						
								layer.msg("修改成功！")
							}
						},
					 	error: function(result) {  //返回数据根据结果进行相应的处理 
					 		layer.msg("修改出错！")
						}
					});
			    	return false;
			  });
			});
	</script>
</body>
</html>