<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
    String path = request.getContextPath(); //web项目的根路径
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>查看历史数据</title>
  <link rel="stylesheet" href="<%=basePath%>static/layui2.4.5/css/layui.css">
<link rel="stylesheet" href="<%=basePath%>static/font-awesome/css/font-awesome.min.css">
<script src="<%=basePath%>static/jquery/jquery.min.js"></script>
<script src="<%=basePath%>static/layui2.4.5/layui.js"></script>
</head>
<body>
<div class="layui-tab layui-tab-brief" lay-filter="docDemoTabBrief">
  <ul class="layui-tab-title">
    <li class="layui-this">血压</li>
    <li>心率</li>
    <li>计步</li>
    <li>睡眠</li>
    <li>血糖</li>
  </ul>
  <div class="layui-tab-content">
  	<div class="layui-tab-item layui-show">
    	<table id="bloodPressure" lay-filter="test01"></table>
    </div>
    <div class="layui-tab-item">
    	<table  id="heartRate" lay-filter="test"></table>
    </div>
    <div class="layui-tab-item">
    	<table id="step" lay-filter="test"></table>
    </div>
    <div class="layui-tab-item">
    	<table id="sleep" lay-filter="test"></table>
    </div>
    <div class="layui-tab-item">
    	<table id="bloodSugger" lay-filter="test"></table>
    </div>
  </div>
</div>     

<script type="text/javascript">
	function child(data){
		var memberId = data.MEMBER_ID
		layui.use('element', function(){
			  var element = layui.element; 
			});
		
		layui.use('table', function() {
			var table = layui.table;
			table.render({
				    elem: '#bloodPressure'
				    ,url: '<%=basePath%>health/bloodPressureByPage?memberId='+memberId //数据接口
				    ,page: true
				    ,limit:5
				    ,limits:[5,10,20]//开启分页
				    ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
				    ,cols: [[
				      {field:'IMEI', width:'25%', title: 'IMEI', align: 'center'}
				      ,{field:'SYSTOLIC_PRESSURE', width:'25%', title: '收缩压' , sort: true ,align: 'center'}
				      ,{field:'DIASTOLIC_PRESSURE',  width:'25%', title: '舒张压', sort: true ,align: 'center'}
				      ,{field:'CREATE_DATE', title: '日期', sort: true ,align: 'center'}
				    ]]
				    
			     });
			table.render({
				    elem: '#heartRate'
				    ,url: '<%=basePath%>health/heartRateByPage?memberId='+memberId //数据接口
				    ,page: true
				    ,limit:5
				    ,limits:[5,10,20]//开启分页
				    ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
				    ,cols: [[
				      {field:'IMEI', width:'35%', title: 'IMEI', align: 'center'}
				      ,{field:'HEART_RATE', width:'35%', title: '心率' ,sort: true, align: 'center'}
				      ,{field:'CREATE_DATE', title: '日期', sort: true ,align: 'center'}
				    ]]
				    
			     });
			table.render({
				    elem: '#step'
				    ,url: '<%=basePath%>health/stepByPage?memberId='+memberId //数据接口
				    ,page: true
				    ,limit:5
				    ,limits:[5,10,20]//开启分页
				    ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
				    ,cols: [[
				      {field:'IMEI', width:'35%', title: 'IMEI', align: 'center'}
				      ,{field:'STEP', width:'35%', title: '步数' ,sort: true, align: 'center'}
				      ,{field:'CREATE_DATE', title: '日期', sort: true ,align: 'center'}
				    ]]
				    
			     });
			table.render({
				    elem: '#sleep'
				    ,url: '<%=basePath%>health/sleepByPage?memberId='+memberId //数据接口
				    ,page: true
				    ,limit:5
				    ,limits:[5,10,20]//开启分页
				    ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
				    ,cols: [[
				      {field:'IMEI', width:'35%', title: 'IMEI', align: 'center'}
				      ,{field:'SLEEP_TIME', width:'35%', title: '睡眠时间', sort: true, align: 'center'}
				      ,{field:'CREATE_DATE', title: '日期', sort: true ,align: 'center'}
				    ]]
				    
			     });
			table.render({
				    elem: '#bloodSugger'
				    ,url: '<%=basePath%>health/bloodSuggerByPage?memberId='+memberId //数据接口
				    ,page: true
				    ,limit:5
				    ,limits:[5,10,20]//开启分页
				    ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
				    ,cols: [[
				      {field:'IMEI', width:'35%', title: 'IMEI', align: 'center'}
				      ,{field:'BLOOD_SUGGER', width:'35%', title: '血糖数值', sort: true, align: 'center'}
				      ,{field:'CREATE_DATE', title: '日期', sort: true ,align: 'center'}
				    ]]
				    
			     });
		});
	}
</script>
</body>
</html>