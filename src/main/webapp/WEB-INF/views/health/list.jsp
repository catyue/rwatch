<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath(); //web项目的根路径
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<title>健康数据</title>
<link rel="stylesheet" href="<%=basePath%>static/layui2.4.5/css/layui.css">
<link rel="stylesheet" href="<%=basePath%>static/font-awesome/css/font-awesome.min.css">
<script src="<%=basePath%>static/jquery/jquery.min.js"></script>
<script src="<%=basePath%>static/layui2.4.5/layui.js"></script>
</head>
<body>
	<div class="layui-inline">
				<div class="layui-inline">
					<label class="layui-form-label"
						style="font-weight: bold; font-size: 15px; width: 70px;">IMEI：</label>
					<div class="layui-input-inline">
						<input type="text" name="waybillnosecondary" autocomplete="off" class="layui-input" id="waybillnosecondary" style="text-transform:uppercase;" onkeyup="this.value=this.value.toUpperCase()"/>
					</div>
				</div>
	</div>
	<div class="layui-inline">
		 <button class="layui-btn" text-align:'right' lay-submit="" id="searchBtn" data-type="getInfo">查询</button>
		 <button class="layui-btn" data-type="reload" id="addBtn">新增</button>
	</div>
	<table class="layui-hide" id="health" lay-filter="test"></table>
	
	<script type="text/html" id="bar">
  		<a class="layui-btn layui-btn-xs" lay-event="edit">修改信息</a>
  		<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="find">查看最新健康数据</a>

	</script>
	
	<script>
		layui.use('table', function() {
			var table = layui.table;
			 table.render({
				    elem: '#health'
				    ,url: '<%=basePath%>health/findListByPage' //数据接口
				    ,page: true
				    ,limit:5
				    ,limits:[5,10,20]//开启分页
				    ,cellMinWidth: 80 //全局定义常规单元格的最小宽度，layui 2.2.1 新增
				    ,cols: [[
				      {field:'IMEI', width:'15%', title: 'IMEI', sort: true ,align: 'center'}
				      ,{field:'USER_NAME', width:'10%', title: '监护人' ,align: 'center'}
				      ,{field:'MEMBER_ID',  width:'10%', title: '会员ID', sort: true ,align: 'center'}
				      ,{field:'MEMBER_NAME',  width:'10%', title: '姓名和号码牌', sort: true ,align: 'center'}
				      ,{field:'MEMBER_LEVEL', width:'15%', title: '会员等级',sort: true,align: 'center'}
				      ,{field:'MONEY', width:'10%', title: '预存金额', sort: true ,align: 'center'}
				      ,{fixed: 'right', title:'操作', toolbar: '#bar',align: 'center'}
				    ]]
				    
			     });
			 table.on('tool(test)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
				  var data = obj.data; //获得当前行数据
				  var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
				  var tr = obj.tr; //获得当前行 tr 的DOM对象
				 
				  if(layEvent === 'find'){ //查看历史
						 layer.open({
							  type: 2,
							  maxmin: true, //开启最大化最小化按钮
						      area: ['893px', '500px'],
						      title: '查看历史数据',
							  content: '<%=basePath%>health/history',
							  success:function(layero,index){
								  var iframe = window['layui-layer-iframe' + index];
								  iframe.child(data);
				                }   
						});
						 $('.layui-laypage-btn').click();
				  }  else if(layEvent === 'edit'){ //修改信息
				    //do something
				        layer.open({
							  type: 2,
							  area: ['400px', '450px'],
							  title: '编辑页面',
							  content: '<%=basePath%>health/toEdit',
							  success:function(layero,index){
								  var iframe = window['layui-layer-iframe' + index];
								  iframe.child(data);
				                }  
							  
						});
				        $('.layui-laypage-btn').click();
				  }
				});
		});
	</script>
</body>
</html>