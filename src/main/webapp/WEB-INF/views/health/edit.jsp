<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%
    String path = request.getContextPath(); //web项目的根路径
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>编辑</title>
  <link rel="stylesheet" href="<%=basePath%>static/layui2.4.5/css/layui.css">
<link rel="stylesheet" href="<%=basePath%>static/font-awesome/css/font-awesome.min.css">
<script src="<%=basePath%>static/jquery/jquery.min.js"></script>
<script src="<%=basePath%>static/layui2.4.5/layui.js"></script>
</head>
<body>
<form class="layui-form" action="">
<div class="layui-form-item">
    <label class="layui-form-label">IMEI</label>
    <div class="layui-input-inline">
      <input type="text"id="imei" name="imei" lay-verify="required"  readonly="readonly" class="layui-input">
    </div>
  </div>
  <div class="layui-form-item">
    <label class="layui-form-label">监护人</label>
    <div class="layui-input-inline">
      <input type="text"id="userName" name="userName" lay-verify="required" readonly="readonly"  class="layui-input">
    </div>
  </div>
  <div class="layui-form-item">
    <label class="layui-form-label">会员Id</label>
    <div class="layui-input-inline">
      <input type="text"id="memberId" name="memberId" lay-verify="required" readonly="readonly"  class="layui-input">
    </div>
  </div>
  <div class="layui-form-item">
    <label class="layui-form-label">会员姓名</label>
    <div class="layui-input-inline">
      <input type="text"id="memberName" name="memberName" lay-verify="required"   class="layui-input">
    </div>
  </div>
  <div class="layui-form-item">
    <label class="layui-form-label">会员等级</label>
    <div class="layui-input-inline">
      <input type="text"id="memberLevel" name="memberLevel" lay-verify="required"   class="layui-input">
    </div>
  </div>
  <div class="layui-form-item">
    <label class="layui-form-label">预存金额</label>
    <div class="layui-input-inline">
      <input type="text" id="money"name="money" lay-verify="required"  class="layui-input">
    </div>
  </div>
  <div class="layui-form-item" id="cl">
		<div class="layui-input-block">
			<input type="button" button lay-submit lay-filter="go" value="确认编辑" class="layui-btn">
		</div>
	</div>
</form>

<script type="text/javascript">
  function child(data){
	  $('#imei').val(data.IMEI);
	  $('#userName').val(data.USER_NAME);
	  $('#memberId').val(data.MEMBER_ID);
	  $('#memberName').val(data.MEMBER_NAME);
	  $('#memberLevel').val(data.MEMBER_LEVEL);
	  $('#money').val(data.MONEY);
  }
  layui.use('form', function(){
      var form = layui.form;
      form.on('submit(go)', function(data){
      console.log(data.field) 
      $.ajax({  
	   	type : "post",  
	   	url : "<%=basePath %>health/updateByMemberId",
	   	data : data.field,
	   	success : function(result) { 
	   		if (result.success) {
	   		    parent.layer.msg('已编辑')
	   			 var index = parent.layer.getFrameIndex(window.name)
	   			 parent.layer.close(index)
	   			 parent.$(".layui-laypage-btn").click() 
	   		} else {  
	   			layer.alert("修改失败")
	   		}  
	   	},
	   	error:function(result){
	   		layer.alert("重试")
	   	}
      });
	return false; 
    	});
	});
</script>
</body>
</html>