<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath(); //web项目的根路径
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <title>发布公告</title>
  <link rel="stylesheet" href="<%=basePath%>static/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="<%= basePath %>static/layui2.4.5/css/layui.css" media="all">
  <style type="text/css">

     .demo-carousel{height: 200px; line-height: 200px; text-align: center;}
	
  	.layui-nav* {
   	 	font-size: 19px !important
	}
	.layui-tab-title .layui-this{
		background-color: #eee
	}
	.layui-form-label{font-size:16px;}
  </style>
  	<script src="<%=basePath %>static/jquery/jquery.min.js"></script>
	<script src="<%=basePath %>static/layui2.4.5/layui.js"></script>
	<script src="<%=basePath %>static/js/login.js"></script>
	<script src="<%= basePath %>static/layui2.4.5/lay/modules/layer.js"></script>
</head>
<body>
<form action="sendmynotice" method="post" class="">
  <div class="layui-form-item">
    <label class="layui-form-label" style="margin-top: 10px;">收件人</label>
    <div class="layui-input-inline">
      <input type="text" name="addresser" id="addresser"   lay-verify="required"  autocomplete="off" class="layui-input" style="margin-top: 10px;">
    </div>
  </div>
  <div class="layui-form-item">
    <label class="layui-form-label">标题</label>
    <div class="layui-input-inline">
      <input type="text" name="title" id="title"   lay-verify="required"  autocomplete="off" class="layui-input">
    </div>
  </div>
  <div class="layui-form-item layui-form-text">
    <label class="layui-form-label">内容</label>
    <div class="layui-input-block">
      <textarea name="content" id="content" placeholder="请输入"  class="layui-textarea" style="width: 522px;height: 114px;"></textarea>
    </div>
  </div>
  <div class="layui-form-item">
    <div class="layui-input-block">
      <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
      <button type="reset" class="layui-btn layui-btn-primary">重置</button>
    </div>
  </div>
</form>
</body>
</html>