<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%
    String path = request.getContextPath(); //web项目的根路径
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<script>
function child(data){
	  $("#title").val(data.title);
	  $("#createDate").val(data.createDate);
	  $("#sender").val(data.sender);
	  $("#addresser").val(data.addresser);
	  $("#content").val(data.content);
	  $("#updateBy").val(data.updateBy);
	  }
</script>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <title>查看公告内容</title>
    <link rel="stylesheet" href="<%= basePath %>static/layui2.4.5/css/layui.css" media="all">
  <script src="<%= basePath %>static/jquery/jquery.min.js"></script>  
  <script src="<%= basePath %>static/layui2.4.5/layui.js"></script>
  <script src="<%= basePath %>static/layui2.4.5/lay/modules/layer.js"></script>
  <style>
    body{margin: 10px;}
    .demo-carousel{height: 200px; line-height: 200px; text-align: center;}
    /* .Content{width:500px} */
    .layui-input-inline
  </style>
</head>
<body>

  <div class="layui-form-item">
    <label class="layui-form-label">标题</label>
    <div class="layui-input-inline">
      <input type="text" name="title" id="title"  readOnly="true" lay-verify="required"  autocomplete="off" class="layui-input">
    </div>
  </div>
    <div class="layui-form-item">
    <label class="layui-form-label">时间</label>
    <div class="layui-input-inline">
      <input type="text" name="createDate" id="createDate" readOnly="true" lay-verify="required"  autocomplete="off" class="layui-input">
    </div>
  </div>
    <div class="layui-form-item">
    <label class="layui-form-label">发件人</label>
    <div class="layui-input-inline">
      <input type="text" name="sender" id="sender" readOnly="true"  lay-verify="required"  autocomplete="off" class="layui-input">
    </div>
  </div>
  <div class="layui-form-item">
    <label class="layui-form-label">收件人</label>
    <div class="layui-input-inline">
      <input type="text" name="addresser" id="addresser" readOnly="true"  lay-verify="required"  autocomplete="off" class="layui-input">
    </div>
  </div>
    <div class="layui-form-item">
    <label class="layui-form-label">最近编辑人</label>
    <div class="layui-input-inline">
      <input type="text" name="updateBy" id="updateBy" readOnly="true"  lay-verify="required"  autocomplete="off" class="layui-input" value="无人编辑">
    </div>
  </div>
  <div class="layui-form-item layui-form-text">
    <label class="layui-form-label">内容</label>
    <div class="layui-input-block">
      <textarea name="content" id="content" readOnly="true" class="layui-textarea"></textarea>
    </div>
  </div>
</body>
</html>