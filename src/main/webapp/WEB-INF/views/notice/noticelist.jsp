<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%
    String path = request.getContextPath(); //web项目的根路径
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <title>查看公告</title>
  <link rel="stylesheet" href="<%= basePath %>static/layui2.4.5/css/layui.css" media="all">
  <style>
    body{margin: 10px;}
    .demo-carousel{height: 200px; line-height: 200px; text-align: center;}
    .layui-table-cell{
    			height:50px;
    			line-height: 50px;
    			font-size:16px;}

  </style>
</head>
<body>
<input type="hidden" id="flag">
<div class="layui-form layui-card-header layuiadmin-card-header-auto">
		<div class="layui-form-item">
		  <div class="layui-inline">
            <label class="layui-form-label">发件人</label>
            <div class="layui-input-inline">
              <input type="text" name="sender" id="sender" placeholder="请输入" autocomplete="off" class="layui-input">
            </div>
          </div>
          <div class="layui-inline">
            <label class="layui-form-label">公告标签</label>
            <div class="layui-input-inline">
              <select name="status" id="status">
               <option value="3">请选择标签</option>
                <option value="0">未读</option>
                <option value="1">已读</option>
              </select><div class="layui-unselect layui-form-select"><div class="layui-select-title"><input type="text" placeholder="请选择标签" value="" readonly="" class="layui-input layui-unselect"><i class="layui-edge"></i></div><dl class="layui-anim layui-anim-upbit"><dd lay-value="" class="layui-select-tips">请选择标签</dd><dd lay-value="0" class="">美食</dd><dd lay-value="1" class="">新闻</dd><dd lay-value="2" class="">八卦</dd><dd lay-value="3" class="">体育</dd><dd lay-value="4" class="">音乐</dd></dl></div>
            </div>
          </div>
          <div class="layui-inline">
            <a class="layui-btn search_btn "  id="searchBtn"  data-type="getInfo" style="margin-left: 15px;"><i class="layui-icon layui-icon-search layuiadmin-button-btn"></i></a>
          </div>
		</div>
      </div>
<table class="layui-hide" id="demo" lay-filter="test"  style="font-size:30px"></table>

<script type="text/html" id="barDemo">
  <a class="layui-btn layui-btn-primary layui-btn-sm " lay-event="detail" id="detail">查看</a>
  <a class="layui-btn layui-btn-danger layui-btn-sm" lay-event="del">删除</a>
</script>
<script src="<%= basePath %>static/jquery/jquery.min.js"></script>  
<script src="<%= basePath %>static/layui2.4.5/layui.js"></script>
<script src="<%= basePath %>static/layui2.4.5/lay/modules/layer.js"></script>
<script>
function test() {
    $.ajax({  
        type : "post",
        datatype:"json",
        url : "<%=basePath %>notice/checkmeg",    //更改已读未读数据接口
        data : {noticeId :$("#flag").val()},                       
        success: function(data){ 
       	   	if(data.success){
       	   	 //do nothing
       	}else{
       		layer.alert(data.msg)
       	}
      },  
      error: function() {    
   	   layer.alert("请检查网络连接");  
      }  
   });
}

layui.config({
  version: '1554901097999' //为了更新 js 缓存，可忽略
});
 
layui.use(['laypage', 'layer', 'table','element'], function(){
   layer = layui.layer //弹层
  ,table = layui.table //表格
  ,element = layui.element //元素操作
  
  //执行一个 table 实例
  table.render({
    elem: '#demo'
    ,height: 410
    ,url: '<%= basePath %>notice/findListByPage' //数据接口 
    ,title: '用户表'
    ,page: true //开启分页
    ,limit:5
    ,limits:[5,10]
    ,toolbar: false   //'default' //开启工具栏，此处显示默认图标，可以自定义模板，详见文档
    ,totalRow: false //开启合计行
    ,cols: [[ //表头
      {field: 'sender', title: '发件人', width: 260, sort: true}
      ,{field: 'title', title: '标题', width:400}
	  ,{field: 'status', title: '状态', width:260, sort: true,templet: function(d){if(d.status=='0'){return '未读'}else{return '已读'}}}
      ,{field: 'createDate', title: '时间', width: 260, sort: true}
 	  ,{title:"操作",width: 475,toolbar: '#barDemo'},
    ]]
  });
  

	  //监听工具条
	    table.on('tool(test)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
	      var data = obj.data; //获得当前行数据
	      var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
	      var tr = obj.tr; //获得当前行 tr 的DOM对象
	      if(layEvent === 'detail'){ //查看
	    	 $("#flag").val(data.noticeId);
	      	 test();
	 		 layer.open({
	 			  type: 2,
	 			  area: ['500px', '380px'],
	 			  content: '<%=basePath%>notice/detail',     //查看操作数据接口
	 			  success:function(layero,index){
	 				  var iframe =window['layui-layer-iframe' + index];
	 				  iframe.child(data);
	                 }
	 		});
	      } else if(layEvent === 'del'){ //删除
	  	    layer.confirm('真的删除行么', function(index){
	  	      //向服务端发送删除指令
	  	      	  //obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
	  	     	layer.close(index);	
	  	  	$.ajax({
	  			type:"get",
	  			url:"<%= basePath %>notice/updateflag",  //删除操作数据接口
	  			data : {
	  				"noticeId":data.noticeId
	  			},
	  			success:function(result){
	  				if(result.success){
	  				
	  					$(".layui-laypage-btn").click()
	  						
	  					}else{
	  						layer.alert("删除失败请重试")
	  					}
	  				},
	  				error:function(result){
	  					layer.alert("删除失败请重试")
	  				}
	  			});
	  	    });
	      } 
	    });
	      //搜索功能
		  $('#searchBtn').on('click',function(){
		        var type = $(this).data('type');
		        active[type] ? active[type].call(this) : '';
		    });
		    // 点击获取数据
		    var  active = {
		 getInfo: function () {
		            var sender=$('#sender').val();
		            var status=$('#status').val();
				    if (status=="3"){
				    	if(sender!=""){	
				    		layer.open({
				    			  title: '提示'
				    			  ,content: '请选择公告标签（未读/已读）'
				    			}); 
			            }
				    	else{
		                table.reload('demo',{
		                	url:'<%= basePath %>notice/findListByPage'
		                		});
				    	}
				    	
				    }
				    else if ($('#sender').val()||$('#status').val()) {	
		                var index = layer.msg('查询中，请稍候...',{icon: 16,time:false,shade:0});
		                setTimeout(function(){
		                    table.reload('demo', {
		                        url:'<%= basePath %>notice/searchnotice'
		                    	,where: {
		                            'sender':sender,
		                            'status':status
		                          }
		                    });
		                    layer.close(index);
		                },800);
		            } else {
		              //  table.reload('demo')
		            		}
		        	},
				};		
			    
	  
	  });
</script>
</body>
</html>