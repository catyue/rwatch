<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%
    String path = request.getContextPath(); //web项目的根路径
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<script>
function child(data){
	  $("#noticeId").val(data.noticeId);
	  $("#title").val(data.title);
	  $("#createDate").val(data.createDate);
	  $("#sender").val(data.sender);
	  $("#addresser").val(data.addresser);
	  $("#content").val(data.content);
	  }
</script>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <title>查看修改公告内容</title>
    <link rel="stylesheet" href="<%= basePath %>static/layui2.4.5/css/layui.css" media="all">
  <script src="<%= basePath %>static/jquery/jquery.min.js"></script>  
  <script src="<%= basePath %>static/layui2.4.5/layui.js"></script>
  <script src="<%= basePath %>static/layui2.4.5/lay/modules/layer.js"></script>
  <style>
    body{margin: 10px;}
    .demo-carousel{height: 200px; line-height: 200px; text-align: center;}
    /* .Content{width:500px} */
  </style>
   <script>
 layui.use('form', function(){
    var form = layui.form;
    form.on('submit(go)', function(data){
    console.log(data.field) //当前容器的全部表单字段，名值对形式：{name: value}
	$.ajax({  
  	type : "post",//提交方式  
  	url : "<%=basePath %>notice/updatenotice",   //更新公告数据接口  
  	data : {"content":$("#content").val(),"noticeId":$("#noticeId").val()},//数据，必须为 Key/Value 格式  
	//data:data.field,    //updateByUserId
  	success : function(result) { //返回数据根据结果进行相应的处理 
  	if (result.success) {
			parent.layer.msg("修改完成",{icon:6})
			var index=parent.layer.getFrameIndex(window.name)
			parent.layer.close(index)
			parent.$(".layui-laypage-btn").click()
		}else {
			layer.alert("修改失败请重试")
		}
	},
	error:function(resule){
		layer.alert("请重试")
  	}  
  });
  });});

 </script>
</head>
<body>
<form>
  <div class="layui-form-item" style="display:none">
    <label class="layui-form-label">ID</label>
    <div class="layui-input-inline">
      <input type="text" name="noticeId" id="noticeId"  readOnly="true" lay-verify="required"  autocomplete="off" class="layui-input" >
    </div>
  </div>
  <div class="layui-form-item">
    <label class="layui-form-label">标题</label>
    <div class="layui-input-inline">
      <input type="text" name="title" id="title"  readOnly="true" lay-verify="required"  autocomplete="off" class="layui-input">
    </div>
  </div>
    <div class="layui-form-item">
    <label class="layui-form-label">时间</label>
    <div class="layui-input-inline">
      <input type="text" name="createDate" id="createDate" readOnly="true" lay-verify="required"  autocomplete="off" class="layui-input">
    </div>
  </div>
    <div class="layui-form-item">
    <label class="layui-form-label">发件人</label>
    <div class="layui-input-inline">
      <input type="text" name="sender" id="sender" readOnly="true"  lay-verify="required"  autocomplete="off" class="layui-input">
    </div>
  </div>
  <div class="layui-form-item">
    <label class="layui-form-label">收件人</label>
    <div class="layui-input-inline">
      <input type="text" name="addresser" id="addresser" readOnly="true"  lay-verify="required"  autocomplete="off" class="layui-input">
    </div>
  </div>
  <div class="layui-form-item layui-form-text">
    <label class="layui-form-label">内容</label>
    <div class="layui-input-block">
      <textarea name="content" id="content" class="layui-textarea"></textarea>
    </div>
  </div>

  <input type="button" button lay-submit lay-filter="go" value="编辑" class="layui-btn" style="margin-left: 110px;">
</form>

  


</body>
</html>