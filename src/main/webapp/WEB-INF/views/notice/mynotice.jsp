<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%
    String path = request.getContextPath(); //web项目的根路径
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <title>发布记录</title>
  <link rel="stylesheet" href="<%= basePath %>static/layui2.4.5/css/layui.css" media="all">
  <style>
    body{margin: 10px;}
    .demo-carousel{height: 200px; line-height: 200px; text-align: center;}
        .layui-table-cell{
    			height:50px;
    			line-height: 50px;
    			font-size:16px;}
    
  </style>
</head>
<body>
<div class="layui-form layui-card-header layuiadmin-card-header-auto">
		<div class="layui-form-item">
		  <div class="layui-inline">
            <label class="layui-form-label">发件人</label>
            <div class="layui-input-inline">
              <input type="text" name="sender" id="sender" placeholder="请输入" autocomplete="off" class="layui-input">
            </div>
          </div>
          <div class="layui-inline">
            <a class="layui-btn search_btn "  id="searchbysender"  data-type="searchbysender" style="margin-left: 15px;"><i class="layui-icon layui-icon-search layuiadmin-button-btn"></i></a>
          </div>
		</div>
      </div>
<table class="layui-hide" id="demo" lay-filter="test"></table>
<script type="text/html" id="barDemo">
  <a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="detail">查看</a>
  <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
  <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>
<script src="<%= basePath %>static/jquery/jquery.min.js"></script>  
<script src="<%= basePath %>static/layui2.4.5/layui.js"></script>
<script src="<%= basePath %>static/layui2.4.5/lay/modules/layer.js"></script>
<script>
layui.config({
  version: '1554901097999' //为了更新 js 缓存，可忽略
});
 
layui.use(['laypage', 'layer', 'table','element'], function(){
   layer = layui.layer //弹层
  ,table = layui.table //表格
  ,element = layui.element //元素操作
  
  //执行一个 table 实例
  table.render({
    elem: '#demo'
    ,height: 410
    ,url: '<%= basePath %>notice/findListByPage' //数据接口
    ,title: '用户表'
    ,page: true //开启分页
    ,limit:5
    ,limits:[5,10]
    ,toolbar: false //开启工具栏，此处显示默认图标，可以自定义模板，详见文档
    ,totalRow: false //开启合计行
    ,cols: [[ //表头
      {type: 'checkbox', fixed: 'left'}
      ,{field: 'title', title: '标题', width:500}
      ,{field: 'sender', title: '发件人', width: 260, sort: true}
      ,{field: 'addresser', title: '收件人', width: 260, sort: true}
      ,{field: 'createDate', title: '时间', width: 260, sort: true}
      ,{title:"操作",width: 320,toolbar: '#barDemo'}
    ]]
  });
  

	  //监听工具条
	    table.on('tool(test)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
	      var data = obj.data; //获得当前行数据
	      var layEvent = obj.event; //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
	      var tr = obj.tr; //获得当前行 tr 的DOM对象
	      if(layEvent === 'detail'){ //查看
	 		 layer.open({
	 			  type: 2,
	 			  area: ['460px', '450px'],
	 			  content: '<%=basePath  %>notice/detail',     //数据接口
	 			  success:function(layero,index){
	 				  var iframe = window['layui-layer-iframe' + index];
	 				  iframe.child(data);
	                 }
	 		});

	      } else if(layEvent === 'del'){ //删除
	  	    layer.confirm('真的删除行么', function(index){
	  	      //向服务端发送删除指令
	  	      	  //obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
	  	     	layer.close(index);	
	  	  	$.ajax({
	  			type:"get",
	  			url:"<%= basePath %>notice/updateflag",  //数据接口
	  			data : {
	  				"noticeId":data.noticeId
	  			},
	  			success:function(result){
	  				if(result.success){
	  				
	  					layer.msg("已删除",{icon:6})
	  					$(".layui-laypage-btn").click()
	  						
	  					}else{
	  						layer.alert("删除失败请重试")
	  					}
	  				},
	  				error:function(result){
	  					layer.alert("删除失败请重试")
	  				}
	  			});
	  	    });
	      } else if(layEvent === 'edit'){ //编辑
		        //do something
	        	layer.open({
	 			  type: 2,
	 			  area: ['460px', '450px'],
	 			  content: '<%=basePath%>notice/edit',
	 			  success:function(layero,index){
	 				  var iframe =window['layui-layer-iframe' + index];
	 				  iframe.child(data);
	                 }
	 		});
	      }
	    });
	  
	      //搜索功能
		  $('#searchbysender').on('click',function(){
		        var type = $(this).data('type');
		        active[type] ? active[type].call(this) : '';
		    });
		    // 点击获取数据
		    var  active = {
		    searchbysender: function () {
		            var sender=$('#sender').val();
					 if ($('#sender').val()) {	
		                var index = layer.msg('查询中，请稍候...',{icon: 16,time:false,shade:0});
		                setTimeout(function(){
		                    table.reload('demo', {
		                        url:'<%= basePath %>notice/findListBysender'
		                    	,where: {
		                            'sender':sender
		                          }
		                    });
		                    layer.close(index);
		                },800);
		            } else {
		              		  table.reload('demo',{
		              		  url:'<%= basePath %>notice/findListByPage'  
		              		  })
		            		}
		        	},
				};		
			    
	  		
	 });
</script>
</body>
</html>