<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%
    String path = request.getContextPath(); //web项目的根路径
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
   
    /*
    	例如：
    	http://localhost:8080/rwatch/login
	    request.getSchema()可以返回当前页面使用的协议，就是上面例子中的“http”
	    request.getServerName()可以返回当前页面所在的服务器的名字，就是上面例子中的“localhost"
	    request.getServerPort()可以返回当前页面所在的服务器使用的端口,就是8080
	    request.getContextPath()可以返回当前页面所在的应用的名字，就是上面例子中的rwatch
	    有了basePath 可以直接用从而进行拼接.
	    
	    */
%>
<!doctype html>
<html lang="zh">
<head>
	<meta charset="UTF-8">
	<title>管理员登录-R-Watch</title>
	<meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link rel="stylesheet" href="<%=basePath%>static/css/font.css">
	<link rel="stylesheet" href="<%=basePath%>static/css/rwatch.css">
	<script src="<%=basePath %>static/jquery/jquery.min.js"></script>
	<script src="<%=basePath%>static/layui2.4.5/layui.js"></script>
<style type="text/css">
.layui-input-inline{
    line-height:34px;
}
</style>
</head>
<body class="login-bg">

 <div class="login">
        <div class="message">R-Watch智能手环注册</div>
        <div id="darkbannerwrap"></div>
        
       <form class="layui-form">
        <div class="layui-input-inline">
            <!-- 用户名 -->
            <div class="layui-inline" style="width: 85%">
                用&nbsp;&nbsp;户&nbsp;&nbsp;名:<input type="text" id="userName" name="userName" required  lay-verify="required" placeholder="请输入用户名" autocomplete="off" class="layui-input">
            </div>
            <!-- 对号 -->
            <div class="layui-inline" style="padding-top: 34px;">
                <i class="layui-icon" id="ri" style="color: green;font-weight: bolder;" hidden></i>
            </div>
            <!-- 错号 -->
            <div class="layui-inline" style="padding-top: 34px;">
                <i class="layui-icon" id="wr" style="color: red; font-weight: bolder;" hidden>ဆ</i>
            </div>
        </div>
            <!-- 密码 -->
        <div class="layui-input-inline">
            <div class="layui-inline" style="width: 85%">
                密&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;码:<input type="password" id="possword" name="password" required  lay-verify="required" placeholder="请输入密码" autocomplete="off" class="layui-input">
            </div>
            <!-- 对号 -->
            <div class="layui-inline" style="padding-top: 34px;">
                <i class="layui-icon" id="pri" style="color: green;font-weight: bolder;" hidden></i>
            </div>
            <!-- 错号 -->
            <div class="layui-inline" style="padding-top: 34px;">
                <i class="layui-icon" id="pwr" style="color: red; font-weight: bolder;" hidden>ဆ</i>
            </div>
        </div>
            <!-- 确认密码 -->
        <div class="layui-input-inline">
            <div class="layui-inline" style="width: 85%">
                确认密码:<input type="password" id="repossword" name="repassword" required  lay-verify="required" placeholder="请确认密码" autocomplete="off" class="layui-input">
            </div>
            <!-- 对号 -->
            <div class="layui-inline" style="padding-top: 34px;">
                <i class="layui-icon" id="rpri" style="color: green;font-weight: bolder;" hidden></i>
            </div>
            <!-- 错号 -->
            <div class="layui-inline" style="padding-top: 34px;">
                <i class="layui-icon" id="repossword" style="color: red; font-weight: bolder;" hidden>ဆ</i>
            </div>
        </div>
        <div class="layui-input-inline">
            <!-- 手机号 -->
            <div class="layui-inline" style="width: 85%">
                手&nbsp;&nbsp;机&nbsp;&nbsp;号:<input type="text" id="mobilePhone" name="mobilePhone" required  lay-verify="required" placeholder="请输入手机号	" autocomplete="off" class="layui-input">
            </div>
            <!-- 对号 -->
            <div class="layui-inline" style="padding-top: 34px;">
                <i class="layui-icon" id="ri" style="color: green;font-weight: bolder;" hidden></i>
            </div>
            <!-- 错号 -->
            <div class="layui-inline" style="padding-top: 34px;">
                <i class="layui-icon" id="wr" style="color: red; font-weight: bolder;" hidden>ဆ</i>
            </div>
        </div>
        
        
        <div class="layui-input-inline login-btn" style="line-height: 72px;width: 100%">
            <button type="submit" lay-submit lay-filter="sub" class="layui-btn" style="width:100%">注册</button>
        </div>
        <hr style="width: 85%" />
        <p style="width: 85%"><a href="<%=basePath %>login/index" class="fl">已有账号？立即登录</a></p>
       <p> <a href="<%=basePath %>login/updataPassword" class="fr">忘记密码？</a></p>
    </form>
    </div>

<script src="<%=basePath %>static/layui2.4.5/layui.js"></script>
<script type="text/javascript">
    layui.use(['form','jquery','layer'], function () {
        var form   = layui.form;
        var $      = layui.jquery;
        var layer  = layui.layer;
        //添加表单失焦事件
        //验证表单
        $('#userName').blur(function() {
            var userName = $(this).val();
            //alert(user);
            $.ajax({
                url:'<%=basePath %>login/checkUser',
                type:'get', //提交方式
                dataType:'json',
                data:{"userName": userName},
                //验证用户名是否可用
                success:function(data){
                    if (data.success) {
                    	$('#ri').removeAttr('hidden');
                        $('#wr').attr('hidden','hidden');
                    } else {
                        $('#wr').removeAttr('hidden');
                        $('#ri').attr('hidden','hidden');
                        layer.msg('当前用户名已被占用! ')
                    }
                }
            })
        });
<%--         $('#mobilePhone').blur(function() {
            var mobilePhone = $(this).val();
            //alert(user);
            $.ajax({
                url:'<%=basePath %>login/checkPhone',
                type:'get', //提交方式
                dataType:'json',
                data:{"mobilePhone": mobilePhone},
                //验证用户名是否可用
                success:function(data){
                    if (data.success) {
                    	$('#ri').removeAttr('hidden');
                        $('#wr').attr('hidden','hidden');
                    } else {
                        $('#wr').removeAttr('hidden');
                        $('#ri').attr('hidden','hidden');
                        layer.msg('当前手机号已被占用! ')
                    }
                }
            })
        }); --%>
        // you code ...
        // 为密码添加正则验证
        $('#possword').blur(function() {
                var reg = /^[\w]{6,12}$/;
                if(!($('#possword').val().match(reg))){
                    //layer.msg('请输入合法密码');
                    $('#pwr').removeAttr('hidden');
                    $('#pri').attr('hidden','hidden');
                    layer.msg('请输入合法密码');
                }else {
                    $('#pri').removeAttr('hidden');
                    $('#pwr').attr('hidden','hidden');
                }
        });
        //验证两次密码是否一致
        $('#repossword').blur(function() {
                if($('#possword').val() != $('#repossword').val()){
                    $('#repossword').removeAttr('hidden');
                    $('#rpri').attr('hidden','hidden');
                    layer.msg('两次输入密码不一致!');
                }else {
                    $('#rpri').removeAttr('hidden');
                    $('#repossword').attr('hidden','hidden');
                };
        });
 
        //
        //添加表单监听事件,提交注册信息
        form.on('submit(sub)', function() {
        	//var userName = $('#userName').val();
            $.ajax({
                url:'<%= basePath %>login/signUser',
                type:'get',
                dataType:'json',
                data:{
                   "userName":$('#userName').val(),
                    "password":$('#possword').val(),
                    "repassword":$('#repossword').val(),
                    "mobilePhone":$('#mobilePhone').val(),
                },
                success:function(data){
                    if (data.success) {
                    	  layer.msg('注册失败');
                          location.href = "<%= basePath %>login/index";
                    }else {
                        layer.msg('注册成功');
                        location.href = "<%= basePath %>login/register";
                    }
                }
            })
            //防止页面跳转
            return false;
        });
 
    });
</script>
</body>
</html>