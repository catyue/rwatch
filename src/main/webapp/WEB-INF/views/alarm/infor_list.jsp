<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>离线消息</title>
<%
    String path = request.getContextPath(); //web项目的根路径
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<script src="<%=basePath %>static/jquery/jquery.min.js"></script>
<script src="<%=basePath%>static/layui2.4.5/layui.js"></script>
<link rel="stylesheet" href="<%=basePath%>static/layui2.4.5/css/layui.css" >
<script >
layui.use('table', function(){
      var table = layui.table;
//      var playerName;
//      if(item != undefined) {
//        playerName=item;
//      }
      table.render({
        elem: '#test' 
        ,url:'<%=basePath %>Comment/inforList'
        ,method:'post'
        /* ,response: {
        	dataName: 'data'
        } */
        ,page:true
        ,limit:5
        /* ,cellMinWidth: 200 */ //全局定义常规单元格的最小宽度，layui 2.2.1 新增
        ,cols: [
               [
                   { field: 'devicename', title: '设备名',align:'center' ,width: "11%", sort: true, fixed: 'left' },
                   { field: 'IMEI', title: 'IMEI',align:'center' , width: "11%" },
                   { field: 'alarmtype', title: '报警类型',align:'center' , width: "11%" },
                   { field: 'alarmtime', title: '报警时间',align:'center' , width: "11%" },
                   { field: 'ontime', title: '定位时间',align:'center' , width: "11%" },
                   { field: 'deviceid', title: '型号',align:'center' , width: "11%" },
                   { field: 'status', title: '状态',align:'center' , width: "11%" },
                   { field: '', title: '操作',align:'center' , width: "11%",toolbar: '#member' },
                   { field: '', title: '操作',align:'center' , width: "11%",toolbar: '#pos' },
               ]
           ],
      });
      
      table.on('tool(test)', function(obj){
  		var data = obj.data;
  		
  		//删除的实现
  		if(obj.event==='member'){
			layer.open({
				type:2,
				area:['469px','413px'],
				content:'<%=basePath %>Comment/toMemberInfor',
				success: function(layero,index){
					var iframe=window['layui-layer-iframe'+index];
					iframe.child(data);
				}
			})
  		}else if(obj.event==='detail'){
			layer.open({
				type:2,
				area:['469px','413px'],
				content:'<%=basePath %>Comment/position?deviceId='+data.deviceid,
				success: function(layero,index){
					var iframe=parent.window['layui-layer-iframe'+index];
					iframe.child(data);
				}
			})
  		}
  	});
});
      
      

      
      
</script>

<script type="text/html" id="member">
  	<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="member">用户信息</a>
</script>
<script type="text/html" id="del">
  	<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">清除</a>
</script>
<script type="text/html" id="pos">
	<a class="layui-btn layui-btn-xs" lay-event="detail">实时跟踪</a>
</script>

</head>

<body>
<table class="layui-hide" id="test" lay-filter="test"></table>



</body>
</html>