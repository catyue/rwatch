<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%
    String path = request.getContextPath(); //web项目的根路径
    String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<script src="<%=basePath %>static/jquery/jquery.min.js"></script>
<script src="<%=basePath%>static/layui2.4.5/layui.js"></script>
<link rel="stylesheet" href="<%=basePath%>static/layui2.4.5/css/layui.css" >
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>layuidetail</title>
<script type="text/javascript">
	function child(data){
		$.ajax({
			type:"get",
			url:"<%=basePath %>Comment/memberInfor",
			data: {'deviceId':data.deviceid},
			success:function(result){
				$('#memberId').val(result.memberId);
				$('#memberAddress').val(result.memberAddress);
				$('#familyNum').val(result.familyNum);
				$('#heathyCondition').val(result.heathyCondition);
				$('#remarks').val(result.remarks);
			},
			error:function(result){
				layer.alert(result.memberId);

			}
		
	});
}
	
</script>
<script type="text/javascript">
	layui.use('form',function(){
		var form=layui.form;
		form.on('submit(go)',function(data){
			console.log(data.field)
			$.ajax({
				type:"post",
				url:"<%=basePath %>Comment/editMember",
				data: data.field,
				success:function(result){
					parent.layer.msg('修改完成',{icon:6});
					var index=parent.layer.getFrameIndex(window.name);
					parent.layer.close(index);
					parent.$(".layui-laypage-btn").click();
					/* if(result.success==="true"){
						layer.alert(success);
					}else{
						
					} */
				},
				error:function(result){
					layer.alert("修改失败")
				}
			});
			return false;
		});
	});
</script>
</head>
<body>
	<form class="layui-form" action="" >
		<div class="layui-form-item">
		    <label class="layui-form-label">用户id</label>
		    <div class="layui-input-inline">
		      	<input id="memberId" type="text" name="memberId" required lay-verify="required" 
		      		placeholder="请输入密码" autocomplete="off" class="layui-input" readonly="readonly">
		    </div>
		    <div class="layui-form-mid layui-word-aux">不能更改</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">家庭地址</label>
			<div class="layui-input-inline">
				<input id="memberAddress" type="text" name="memberAddress" required lay-verify="required"
					placeholder='' autocomplete="off" class="layui-input">
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">亲情号码</label>
			<div class="layui-input-inline">
				<input id="familyNum" type="text" name="familyNum" required lay-verify="required"
					placeholder='' autocomplete="off" class="layui-input">
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">健康状况</label>
			<div class="layui-input-inline">
				<input id="heathyCondition" type="text" name="heathyCondition" required lay-verify="required"
					placeholder='' autocomplete="off" class="layui-input">
			</div>
		</div>
		<div class="layui-form-item">
			<label class="layui-form-label">客户信息</label>
			<div class="layui-input-inline">
				<input id="remarks" type="text" name="remarks" required lay-verify="required"
					placeholder='' autocomplete="off" class="layui-input">
			</div>
		</div>
		<div class="layui-form-item" id="cl">
			<div class="layui-input-block">
				<input type="button" button lay-submit lay-filter="go" value="编辑" class="layui-btn">
			</div>
		</div>
	</form>
</body>
</html>