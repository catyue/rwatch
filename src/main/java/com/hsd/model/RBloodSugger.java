package com.hsd.model;

import java.util.Date;

public class RBloodSugger {
    private Integer bloodSugerId;

    private Integer deviceinforId;

    private String bloodSugger;

    private String createBy;

    private Date createDate;

    private String updateBy;

    private Date updateDate;

    public Integer getBloodSugerId() {
        return bloodSugerId;
    }

    public void setBloodSugerId(Integer bloodSugerId) {
        this.bloodSugerId = bloodSugerId;
    }

    public Integer getDeviceinforId() {
        return deviceinforId;
    }

    public void setDeviceinforId(Integer deviceinforId) {
        this.deviceinforId = deviceinforId;
    }

    public String getBloodSugger() {
        return bloodSugger;
    }

    public void setBloodSugger(String bloodSugger) {
        this.bloodSugger = bloodSugger == null ? null : bloodSugger.trim();
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy == null ? null : updateBy.trim();
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}