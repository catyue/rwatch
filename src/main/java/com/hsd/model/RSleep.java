package com.hsd.model;

import java.util.Date;

public class RSleep {
    private Integer sleepId;

    private Integer deviceinforId;

    private String sleepTime;

    private String createBy;

    private Date createDate;

    private String updateBy;

    private Date updateDate;

    public Integer getSleepId() {
        return sleepId;
    }

    public void setSleepId(Integer sleepId) {
        this.sleepId = sleepId;
    }

    public Integer getDeviceinforId() {
        return deviceinforId;
    }

    public void setDeviceinforId(Integer deviceinforId) {
        this.deviceinforId = deviceinforId;
    }

    public String getSleepTime() {
        return sleepTime;
    }

    public void setSleepTime(String sleepTime) {
        this.sleepTime = sleepTime == null ? null : sleepTime.trim();
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy == null ? null : updateBy.trim();
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}