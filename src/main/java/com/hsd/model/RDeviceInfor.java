package com.hsd.model;

import java.util.Date;

public class RDeviceInfor {
    private Integer deviceinforId;

    private Integer deviceId;

    private String deviceStatus;

    private Double latLatitude;

    private Double lngLongitude;

    private Integer devicePower;

    private Date uploadTime;

    private Integer delFlag;

    private String createBy;

    private Date createDate;

    private String updateBy;

    private Date updateDate;

    public Integer getDeviceinforId() {
        return deviceinforId;
    }

    public void setDeviceinforId(Integer deviceinforId) {
        this.deviceinforId = deviceinforId;
    }

    public Integer getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceStatus() {
        return deviceStatus;
    }

    public void setDeviceStatus(String deviceStatus) {
        this.deviceStatus = deviceStatus == null ? null : deviceStatus.trim();
    }

    public Double getLatLatitude() {
        return latLatitude;
    }

    public void setLatLatitude(Double latLatitude) {
        this.latLatitude = latLatitude;
    }

    public Double getLngLongitude() {
        return lngLongitude;
    }

    public void setLngLongitude(Double lngLongitude) {
        this.lngLongitude = lngLongitude;
    }

    public Integer getDevicePower() {
        return devicePower;
    }

    public void setDevicePower(Integer devicePower) {
        this.devicePower = devicePower;
    }

    public Date getUploadTime() {
        return uploadTime;
    }

    public void setUploadTime(Date uploadTime) {
        this.uploadTime = uploadTime;
    }

    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy == null ? null : updateBy.trim();
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}