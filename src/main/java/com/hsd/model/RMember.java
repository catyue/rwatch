package com.hsd.model;

import java.util.Date;

public class RMember {
    private Integer memberId;

    private String memberName;

    private Integer memberLevel;

    private String memberPhone;

    private Integer memberAge;

    private Double memberHeight;

    private Double memberWeight;

    private String memberGroup;

    private String memberAddress;

    private String familyNum;

    private String heathyCondition;

    private String money;

    private Integer userId;

    private String remarks;

    private Integer delFlag;

    private String createBy;

    private Date createDate;

    private String updateBy;

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName == null ? null : memberName.trim();
    }

    public Integer getMemberLevel() {
        return memberLevel;
    }

    public void setMemberLevel(Integer memberLevel) {
        this.memberLevel = memberLevel;
    }

    public String getMemberPhone() {
        return memberPhone;
    }

    public void setMemberPhone(String memberPhone) {
        this.memberPhone = memberPhone == null ? null : memberPhone.trim();
    }

    public Integer getMemberAge() {
        return memberAge;
    }

    public void setMemberAge(Integer memberAge) {
        this.memberAge = memberAge;
    }

    public Double getMemberHeight() {
        return memberHeight;
    }

    public void setMemberHeight(Double memberHeight) {
        this.memberHeight = memberHeight;
    }

    public Double getMemberWeight() {
        return memberWeight;
    }

    public void setMemberWeight(Double memberWeight) {
        this.memberWeight = memberWeight;
    }

    public String getMemberGroup() {
        return memberGroup;
    }

    public void setMemberGroup(String memberGroup) {
        this.memberGroup = memberGroup == null ? null : memberGroup.trim();
    }

    public String getMemberAddress() {
        return memberAddress;
    }

    public void setMemberAddress(String memberAddress) {
        this.memberAddress = memberAddress == null ? null : memberAddress.trim();
    }

    public String getFamilyNum() {
        return familyNum;
    }

    public void setFamilyNum(String familyNum) {
        this.familyNum = familyNum == null ? null : familyNum.trim();
    }

    public String getHeathyCondition() {
        return heathyCondition;
    }

    public void setHeathyCondition(String heathyCondition) {
        this.heathyCondition = heathyCondition == null ? null : heathyCondition.trim();
    }

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money == null ? null : money.trim();
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks == null ? null : remarks.trim();
    }

    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy == null ? null : updateBy.trim();
    }
}