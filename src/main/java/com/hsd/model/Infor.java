package com.hsd.model;

import java.util.Date;

public class Infor {
    private String devicename;
    private String IMEI;
    private String alarmtype;
    private String alarmtime;
    private String ontime;
    private String deviceid;
    private String status;
    public String getDevicename() {
        return devicename;
    }
    
    public void setDevicename(String devicename) {
        this.devicename = devicename;
    }
    
    public String getIMEI() {
        return IMEI;
    }
    
    public void setIMEI(String iMEI) {
        IMEI = iMEI;
    }
    
    public String getAlarmtype() {
        return alarmtype;
    }
    
    public void setAlarmtype(String alarmtype) {
        this.alarmtype = alarmtype;
    }
    
    public String getAlarmtime() {
        return alarmtime;
    }
    
    public void setAlarmtime(String alarmtime) {
        this.alarmtime = alarmtime;
    }
    
    public String getOntime() {
        return ontime;
    }
    
    public void setOntime(String ontime) {
        this.ontime = ontime;
    }
    
    public String getDeviceid() {
        return deviceid;
    }
    
    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }
    
    public String getStatus() {
        return status;
    }
    
    public void setStatus(String status) {
        this.status = status;
    }
   
    
    
}
