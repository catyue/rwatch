package com.hsd.model;

import java.util.Date;

public class RFence {
    private Integer fenceId;

    private Integer deviceId;

    private Integer memberId;

    private Double latLatitude;

    private Double lngLongitude;

    private Double radius;

    private Date fenceTime;

    private Integer delFlag;

    private String createBy;

    private Date createDate;

    private String updateBy;

    private Date updateDate;

    public Integer getFenceId() {
        return fenceId;
    }

    public void setFenceId(Integer fenceId) {
        this.fenceId = fenceId;
    }

    public Integer getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    public Integer getMemberId() {
        return memberId;
    }

    public void setMemberId(Integer memberId) {
        this.memberId = memberId;
    }

    public Double getLatLatitude() {
        return latLatitude;
    }

    public void setLatLatitude(Double latLatitude) {
        this.latLatitude = latLatitude;
    }

    public Double getLngLongitude() {
        return lngLongitude;
    }

    public void setLngLongitude(Double lngLongitude) {
        this.lngLongitude = lngLongitude;
    }

    public Double getRadius() {
        return radius;
    }

    public void setRadius(Double radius) {
        this.radius = radius;
    }

    public Date getFenceTime() {
        return fenceTime;
    }

    public void setFenceTime(Date fenceTime) {
        this.fenceTime = fenceTime;
    }

    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy == null ? null : updateBy.trim();
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}