package com.hsd.model;

import java.util.Date;

public class RBloodPressure {
    private Integer 
bloodPressureId;

    private Integer deviceinforId;

    private String diastolicPressure;

    private String systolicPressure;

    private String createBy;

    private Date createDate;

    private String updateBy;

    private Date updateDate;

    public Integer getbloodPressureId() {
        return bloodPressureId;
    }

    public void setbloodPressureId(Integer bloodPressureId) {
        this.bloodPressureId = bloodPressureId;
    }

    public Integer getDeviceinforId() {
        return deviceinforId;
    }

    public void setDeviceinforId(Integer deviceinforId) {
        this.deviceinforId = deviceinforId;
    }

    public String getDiastolicPressure() {
        return diastolicPressure;
    }

    public void setDiastolicPressure(String diastolicPressure) {
        this.diastolicPressure = diastolicPressure == null ? null : diastolicPressure.trim();
    }

    public String getSystolicPressure() {
        return systolicPressure;
    }

    public void setSystolicPressure(String systolicPressure) {
        this.systolicPressure = systolicPressure == null ? null : systolicPressure.trim();
    }

    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy == null ? null : createBy.trim();
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy == null ? null : updateBy.trim();
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}