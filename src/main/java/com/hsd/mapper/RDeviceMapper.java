package com.hsd.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.hsd.model.RDevice;

public interface RDeviceMapper {
    int deleteByPrimaryKey(Integer deviceid);

    int insert(RDevice record);

    int insertSelective(RDevice record);

    RDevice selectByPrimaryKey(Integer deviceid);

    int updateByPrimaryKeySelective(RDevice record);

    int updateByPrimaryKey(RDevice record);
    
    
    List<RDevice> aDevicesAll(@Param(value="start")int start,@Param(value="end") int end);
	List<RDevice> findDeviceExp(@Param(value="start")int start,@Param(value="end") int end,@Param(value="time") int time,@Param(value="deviceName") String deviceName);
    int getCountAll();
	
	List<RDevice> aDeviceExpData(@Param(value="start")int start,@Param(value="end") int end,@Param(value="time") int time);
	List<RDevice> findDeviceExpData(@Param(value="start")int start,@Param(value="end") int end,@Param(value="time") int time, @Param(value="deviceName") String deviceName);
	int getCountData(@Param(value="time") int time);
	
	List<RDevice> aDeviceExp(@Param(value="start")int start,@Param(value="end") int end);
	List<RDevice> findAllDeviceExp(@Param(value="start")int start,@Param(value="end") int end, @Param(value="deviceName") String deviceName);
	int getCount();


}