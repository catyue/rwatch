package com.hsd.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.hsd.model.RMember;

public interface HealthMapper {

    Integer getCount();

    List<Map<String, Object>> findListByPage(@Param(value="start")Integer start, @Param(value="end")Integer end);

    void updateByMemberId(@Param(value="memberId")String memberId, 
            @Param(value="memberName")String memberName, 
            @Param(value="memberLevel")String memberLevel, 
            @Param(value="money")String money);

    List<Map<String, Object>> bloodPressureByPage(@Param(value="start")Integer start, 
            @Param(value="end")Integer end, 
            @Param(value="memberId")Integer memberId);
    
    List<Map<String, Object>> heartRateByPage(@Param(value="start")Integer start, 
            @Param(value="end")Integer end, 
            @Param(value="memberId")Integer memberId);
    
    List<Map<String, Object>> stepByPage(@Param(value="start")Integer start, 
            @Param(value="end")Integer end, 
            @Param(value="memberId")Integer memberId);
    
    List<Map<String, Object>> sleepByPage(@Param(value="start")Integer start, 
            @Param(value="end")Integer end, 
            @Param(value="memberId")Integer memberId);
    
    List<Map<String, Object>> bloodSuggerByPage(@Param(value="start")Integer start, 
            @Param(value="end")Integer end, 
            @Param(value="memberId")Integer memberId);

    
}