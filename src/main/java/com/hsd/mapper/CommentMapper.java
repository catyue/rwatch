package com.hsd.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestParam;

import com.hsd.model.RUser;

public interface CommentMapper {
    //
    List<Map<String, Object>> listInfor();
    List<Map<String, Object>> listPageInfor(@Param(value="s")int s,
            @Param(value="e")int e);

    //
    List<Map<String, Object>> offlineInfor();
    List<Map<String, Object>> pageOfflineInfor(@Param(value="s")int s,
            @Param(value="e")int e);
    
    //
    List<Map<String, Object>> unInfor();
    List<Map<String, Object>> pageUnInfor(@Param(value="s")int s,
            @Param(value="e")int e);

    //
    List<Map<String, Object>> inthreeInfor();
    List<Map<String, Object>> pageInthreeInfor(@Param(value="s")int s,
            @Param(value="e")int e);
    
    //
    List<Map<String, Object>> listPosition(String deviceId);
    
    //
    public int deleteAlarmByIMEIAndAlarmTime(@Param(value="deviceId")String deviceId,
            @Param(value="alarmtime") String alarmtime,@Param(value="ontime")String ontime);
    
    //
    RUser findUserByDeviceId(String deviceId);

    
}
