package com.hsd.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.hsd.model.RMember;

public interface RMemberMapper {
    int insert(RMember record);

    int insertSelective(RMember record);

    RMember findRMemberByDeviceId(@Param(value="deviceId")String deviceId);

    int updateByPrimaryKeySelective(RMember r);
}