package com.hsd.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.hsd.model.RNotice;
import com.hsd.model.RUser;

public interface RNoticeMapper {
    int deleteByPrimaryKey(Integer noticeId);

    int insert(RNotice record);

    int insertSelective(RNotice record);

    RNotice selectByPrimaryKey(Integer noticeId);

    int updateByPrimaryKeySelective(RNotice record);

    int updateByPrimaryKey(RNotice record);

    int getCount();

    List<RUser> findListByPage(@Param(value="start") int start,@Param(value="end") int end);

    void updateflag(@Param(value="noticeId")int noticeId);

    void updatenotice(@Param(value="content")String content, @Param(value="noticeId")int noticeId, @Param(value="updateBy")String updateBy);

    void selectbynoticeidcheckmeg(@Param(value="noticeId")int noticeId);

    void sendmynotice(@Param(value="addresser")String addresser, @Param(value="title")String title,@Param(value="content") String content,@Param(value="sender") String sender);

    List<RUser> findListBysender(@Param(value="start") int start,@Param(value="end") int end, @Param(value="sender")String sender);

    List<RUser> searchUserByName(@Param(value="sender")String sender);

    List<RUser> searchnoticebysender(@Param(value="start") int start,@Param(value="end") int end, @Param(value="sender")String sender,@Param(value="status") String status);

    int getCountBysender(@Param(value="sender")String sender);

    List<RUser> searchnoticebystatus(@Param(value="start") int start,@Param(value="end") int end,@Param(value="status") String status);
    
    int getCountBystatus(@Param(value="status")String status);





}