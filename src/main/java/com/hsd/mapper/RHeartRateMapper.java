package com.hsd.mapper;

import com.hsd.model.RHeartRate;

public interface RHeartRateMapper {
    int deleteByPrimaryKey(Integer heartRateId);

    int insert(RHeartRate record);

    int insertSelective(RHeartRate record);

    RHeartRate selectByPrimaryKey(Integer heartRateId);

    int updateByPrimaryKeySelective(RHeartRate record);

    int updateByPrimaryKey(RHeartRate record);
}