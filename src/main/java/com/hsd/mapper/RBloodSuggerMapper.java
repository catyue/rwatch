package com.hsd.mapper;

import com.hsd.model.RBloodSugger;

public interface RBloodSuggerMapper {
    int deleteByPrimaryKey(Integer bloodSugerId);

    int insert(RBloodSugger record);

    int insertSelective(RBloodSugger record);

    RBloodSugger selectByPrimaryKey(Integer bloodSugerId);

    int updateByPrimaryKeySelective(RBloodSugger record);

    int updateByPrimaryKey(RBloodSugger record);
}