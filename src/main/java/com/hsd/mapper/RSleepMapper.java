package com.hsd.mapper;

import com.hsd.model.RSleep;

public interface RSleepMapper {
    int deleteByPrimaryKey(Integer sleepId);

    int insert(RSleep record);

    int insertSelective(RSleep record);

    RSleep selectByPrimaryKey(Integer sleepId);

    int updateByPrimaryKeySelective(RSleep record);

    int updateByPrimaryKey(RSleep record);
}