package com.hsd.mapper;

import com.hsd.model.RStep;

public interface RStepMapper {
    int deleteByPrimaryKey(Integer stepId);

    int insert(RStep record);

    int insertSelective(RStep record);

    RStep selectByPrimaryKey(Integer stepId);

    int updateByPrimaryKeySelective(RStep record);

    int updateByPrimaryKey(RStep record);
}