package com.hsd.mapper;

import com.hsd.model.RBloodPressure;

public interface RBloodPressureMapper {
    int deleteByPrimaryKey(Integer bloodPressureId);

    int insert(RBloodPressure record);

    int insertSelective(RBloodPressure record);

    RBloodPressure selectByPrimaryKey(Integer bloodPressureId);

    int updateByPrimaryKeySelective(RBloodPressure record);

    int updateByPrimaryKey(RBloodPressure record);
}