package com.hsd.mapper;

import org.apache.ibatis.annotations.Param;

import com.hsd.model.RUser;

public interface UserMapper {
    int deleteByPrimaryKey(Integer userId);

    int insert(RUser record);

    int insertSelective(RUser record);

    RUser selectByPrimaryKey(Integer userId);

    int updateByPrimaryKeySelective(RUser record);

    int updateByPrimaryKey(RUser record);

    RUser findUser(@Param(value="username")String username, @Param(value="password")String password);

    
}