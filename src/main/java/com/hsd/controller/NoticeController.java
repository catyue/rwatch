package com.hsd.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import javax.servlet.http.HttpServletRequest;
import com.hsd.model.RUser;
import com.hsd.service.RNoticeService;
import com.hsd.util.AjaxJson;
import com.hsd.util.JsonConversion;


@Controller
@RequestMapping("/notice")
public class NoticeController {
         
    @Autowired
    private RNoticeService RNoticeService;
    
   @RequestMapping("/noticelist")
    public String noticelist(){
        return "notice/noticelist";
    } 

        
  

    @RequestMapping("/sendnotice")
    public String sendnotice(){
        return "notice/sendnotice";
    }  

    @RequestMapping("/mynotice")
    public String mynotice(){
        return "notice/mynotice";
    } 
    
    //查看公告
    @ResponseBody
    @RequestMapping("/detail")    //查看
    public ModelAndView detail(){      
        ModelAndView mav = new ModelAndView("notice/detailnotice");   
        return mav;    
    } 


    //进入编辑页面
    @RequestMapping("/edit")    //进入编辑框
    public ModelAndView tobian(){      
        ModelAndView mav = new ModelAndView("notice/editnotice");   
        return mav;    
    } 
    
    //显示所有公告并分页
    @ResponseBody  
    @RequestMapping(value="/findListByPage",produces="application/json; charset=UTF-8")
    public String findListByPage(
            @RequestParam(value="page")int page,
            @RequestParam(value="limit")int limit,
            Model model,
            HttpServletRequest request)
    {
        int start = (page-1)*limit + 1;
        int end = page*limit;
        int count = RNoticeService.getCount();
       List<RUser> userList = RNoticeService.findListByPage(start,end);
     
       Map<Object, Object> map = new HashMap<Object, Object>();
       if (userList!=null && userList.size()>0) {
           map.put("code", 0);
           map.put("msg", "success");
           map.put("count", count);
           map.put("data", userList);
       }else {
           map.put("code", -1);
           map.put("msg", "无数据");
           map.put("data", "");
       }
       return  JsonConversion.writeMapJSON(map);
    }
   
    //删除操作
    @ResponseBody
    @RequestMapping(value="/updateflag")
    public AjaxJson updateflag(@RequestParam(value="noticeId")int noticeId){
        AjaxJson j=new AjaxJson();
        try{
            RNoticeService.updateflag(noticeId);
            j.setMsg("删除成功");
            j.setSuccess(true);
            }catch(Exception e){
                e.printStackTrace();
                j.setSuccess(false);
            }
        return j;
    } 
    
    //编辑操作
    @ResponseBody
    @RequestMapping(value="/updatenotice")  
    public AjaxJson updatenotice(HttpSession session,@RequestParam(value="content") String content,@RequestParam(value="noticeId") int noticeId){
                AjaxJson j=new AjaxJson();
                RUser user = (RUser) session.getAttribute("user");
                String updateBy = user.getUserName();
                try{
                    RNoticeService.updatenotice(content,noticeId,updateBy);
                    j.setMsg("编辑成功");
                    j.setSuccess(true);
                }catch(Exception e){
                    e.printStackTrace();
                    j.setSuccess(false);
                }
                return j;   
        
    }
    
    //未读变已读操作
    @RequestMapping(value ="/checkmeg", method = RequestMethod.POST)
    @ResponseBody
    public AjaxJson checkmsg(String noticeId,HttpSession session){
        AjaxJson j = new AjaxJson();
        int notice = Integer.valueOf(noticeId);  //转换类型string到int类型
        System.out.println(notice);
        RNoticeService.selectbynoticeidcheckmeg(notice);
        return j;
    }
    

        
        //发布操作
          @RequestMapping(value="/sendmynotice",method=RequestMethod.POST)    
            public String sendmynotice(HttpSession session,@RequestParam(value="addresser")String addresser,@RequestParam(value="title") String title,@RequestParam(value="content") String content){      
            ModelAndView mav = new ModelAndView("sender");  
            RUser user = (RUser) session.getAttribute("user");
            String sender = user.getUserName();
            RNoticeService.sendmynotice(addresser,title,content,sender);
            return "redirect:/notice/noticelist";   
        } 
          
          
          //根据sender查询发布的所有公告
          @ResponseBody  
          @RequestMapping(value="/findListBysender",produces="application/json; charset=UTF-8")
          public String findListBysender(
                  HttpSession session,
                  @RequestParam(value="page")int page,
                  @RequestParam(value="limit")int limit,
                  HttpServletRequest request, 
                  Model model)
          {   
              String sender = request.getParameter("sender");//获取浏览器携带过来的参数sender
              int start = (page-1)*limit + 1;
              int end = page*limit;
              int count = RNoticeService.getCountBysender(sender);
             List<RUser> userList = RNoticeService.findListBysender(start,end,sender);
             Map<Object, Object> map = new HashMap<Object, Object>();
             if (userList!=null && userList.size()>0) {
                 map.put("code", 0);
                 map.put("msg", "success");
                 map.put("count", count);
                 map.put("data", userList);
             }else {
                 map.put("code", -1);
                 map.put("msg", "该管理员没有发布过公告");
                 map.put("data", "");
             }
             return  JsonConversion.writeMapJSON(map);
          }
          
          // 搜索功能
          //根据发信人和公告状态
          @ResponseBody
          @RequestMapping(value="/searchnotice",produces = "application/json; charset=UTF-8")
          public String findListByPage(
                  @RequestParam(value="page")int page,
                  @RequestParam(value="limit")int limit,
                  HttpServletRequest request, 
                  Model model)
          {
              String sender = request.getParameter("sender");//获取浏览器携带过来的参数sender
              String status = request.getParameter("status");//获取浏览器携带过来的参数status
              int start = (page-1)*limit + 1; //分页操作 见找规律
              int end = page*limit;
              int count = 0;
              List<RUser> userList = null;
              if(sender!=null&&sender!=""){
                  userList = RNoticeService.searchnoticebysender(start,end,sender,status);
                  count = RNoticeService.getCountBysender(sender);
              }
              else{ 
                  userList = RNoticeService. searchnoticebystatus(start,end,status);
                  count = RNoticeService.getCountBystatus(status);
              }
             //拼接成json字符串  满足layui定义的表格渲染格式
             Map map = new HashMap<>();
             if (userList!=null && userList.size()>0) {
                 map.put("code", 0);
                 map.put("msg", "success");
                 map.put("count", count);
                 map.put("data", userList);
             }else {
                 map.put("code", -1);
                 map.put("msg", "无数据");
                 map.put("data", "");
             }
             return  JsonConversion.writeMapJSON(map);
          }
          
       
}
