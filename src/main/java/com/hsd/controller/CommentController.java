package com.hsd.controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hsd.model.RUser;
import com.hsd.model.Infor;
import com.hsd.model.RMember;
import com.hsd.service.CommentService;
import com.hsd.util.MapListToList;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@Controller
@RequestMapping("/Comment")
public class CommentController {
     
    @Autowired
    private CommentService commentService;
    //所有消息
    @RequestMapping("/toInforList")    
    public ModelAndView toInforList(HttpSession session){      
        ModelAndView mav = new ModelAndView("alarm/infor_list");   
        return mav;    
    }
    @ResponseBody
    @RequestMapping(value="/inforList",method=RequestMethod.POST)    
     public String inforList(HttpSession session,@RequestParam(value="page")int page,
             @RequestParam(value="limit")int limit){
        int s=0;
        int e=0;
        s=(page-1)*limit;
        e=limit;
        JSONObject j = new JSONObject();
        List<Map<String, Object>> list1 = commentService.listInfor();
        List<Map<String, Object>> list = commentService.listPageInfor(s,e);
        MapListToList t=new MapListToList();
        List<Infor> inforList=t.toList(list);
        JSONArray ar=JSONArray.fromObject(inforList);
        System.out.println(ar);
        Map<String, Object> result = new HashMap();
        result.put("code", 0);
        result.put("msg", "");
        result.put("count",list1.size() );
        result.put("data", ar);
        // 将其转换为JSON数据，并压入值栈返回
         j=JSONObject.fromObject(result);
         return j.toString();    
     }
    
    //实时定位
    @RequestMapping("/position")    
    public ModelAndView toPosition(HttpSession session,HttpServletRequest request){
        ModelAndView mav = new ModelAndView("alarm/position_list");
        String deviceId=request.getParameter("deviceId");
        List<Map<String, Object>> list = commentService.listPosition(deviceId);
        mav.addObject("list", list);
        return mav;    
    }
    
    //修改处理状态（清除）
    @RequestMapping(value="/deleteAlarm",method=RequestMethod.GET)    
    @ResponseBody
    public String deleteAlarm(HttpSession session,@RequestParam(value="deviceid")String deviceId,@RequestParam(value="alarmtime")String alarmtime,
            @RequestParam(value="ontime")String ontime){  
        Boolean result= commentService.deleteAlarmByIMEIAndAlarmTime(deviceId,alarmtime,ontime);
        System.out.println(result);
        JSONObject j = new JSONObject();
        j.put("result", result);
        System.out.println(j);
        return j.toString();
    }
    
    //用户信息
    @RequestMapping("/toMemberInfor")    
    public ModelAndView toMemberList(HttpSession session){      
        ModelAndView mav = new ModelAndView("alarm/member_infor");   
        return mav;    
    }
    @ResponseBody
    @RequestMapping(value="/memberInfor",method=RequestMethod.GET)    
    public JSONObject memberList(HttpSession session,@RequestParam(value="deviceId")String deviceId){
        RMember rMember= commentService.findRMemberByDeviceId(deviceId);
        JSONObject j = new JSONObject();
        j.put("memberId", rMember.getMemberId());
        j.put("memberAddress", rMember.getMemberAddress());
        j.put("familyNum", rMember.getFamilyNum());
        j.put("heathyCondition", rMember.getHeathyCondition());
        j.put("remarks", rMember.getRemarks());
        System.out.println(j);
        return j;
    }
    //用户信息修改
    @ResponseBody
    @RequestMapping(value="/editMember",method=RequestMethod.POST)    
    public JSONObject editMember(HttpSession session,
            @RequestParam(value="memberId")int memberId,
            @RequestParam(value="memberAddress")String memberAddress,
            @RequestParam(value="familyNum")String familyNum,
            @RequestParam(value="heathyCondition")String heathyCondition,
            @RequestParam(value="remarks")String remarks){
        RMember r = new RMember();
        r.setMemberId(memberId);
        r.setMemberAddress(memberAddress);
        r.setFamilyNum(familyNum);
        r.setHeathyCondition(heathyCondition);
        r.setRemarks(remarks);
        boolean result=commentService.editMember(r);
        JSONObject j = new JSONObject();
        j.put("rusult", result);
        return j;
    }
    //未处理消息
    @RequestMapping("/toUnInfor")    
    public ModelAndView toUnInfor(HttpSession session){      
        ModelAndView mav = new ModelAndView("alarm/uninfor");   
        return mav;    
    }
    @ResponseBody
    @RequestMapping(value="/unInfor",method=RequestMethod.POST)    
     public String unInfor(HttpSession session,@RequestParam(value="page")int page,
             @RequestParam(value="limit")int limit){
        int s=0;
        int e=0;
        s=(page-1)*limit;
        e=limit;
        JSONObject j = new JSONObject();
        List<Map<String, Object>> list1 = commentService.unInfor();
        List<Map<String, Object>> list = commentService.pageUnInfor(s,e);
        MapListToList t=new MapListToList();
        List<Infor> inforList=t.toList(list);
        JSONArray ar=JSONArray.fromObject(inforList);
        System.out.println(ar);
        Map<String, Object> result = new HashMap();
        result.put("code", 0);
        result.put("msg", "");
        result.put("count",list1.size() );
        result.put("data", ar);
        // 将其转换为JSON数据，并压入值栈返回
         j=JSONObject.fromObject(result);
         return j.toString();    
     }
    //离线未处理消息
    @RequestMapping("/toOfflineInfor")    
    public ModelAndView toOfflineInfor(HttpSession session){      
        ModelAndView mav = new ModelAndView("alarm/offline_infor");   
        return mav;    
    }
    @ResponseBody
    @RequestMapping(value="/offlineInfor",method=RequestMethod.POST)    
     public String offlineInfor(HttpSession session,@RequestParam(value="page")int page,
             @RequestParam(value="limit")int limit){
        int s=0;
        int e=0;
        s=(page-1)*limit;
        e=limit;
        JSONObject j = new JSONObject();
        List<Map<String, Object>> list1 = commentService.offlineInfor();
        List<Map<String, Object>> list = commentService.pageOfflineInfor(s,e);
        MapListToList t=new MapListToList();
        List<Infor> inforList=t.toList(list);
        JSONArray ar=JSONArray.fromObject(inforList);
        System.out.println(ar);
        Map<String, Object> result = new HashMap();
        result.put("code", 0);
        result.put("msg", "");
        result.put("count",list1.size() );
        result.put("data", ar);
        // 将其转换为JSON数据，并压入值栈返回
         j=JSONObject.fromObject(result);
         return j.toString();    
     }
    //三天内消息
    @RequestMapping("/toInthreeInfor")    
    public ModelAndView toInthreeInfor(HttpSession session){      
        ModelAndView mav = new ModelAndView("alarm/inthreeinfor");   
        return mav;    
    }
    @ResponseBody
    @RequestMapping(value="/inthreeInfor",method=RequestMethod.POST)    
     public String inthreeInfor(HttpSession session,@RequestParam(value="page")int page,
             @RequestParam(value="limit")int limit){
        int s=0;
        int e=0;
        s=(page-1)*limit;
        e=limit;
        JSONObject j = new JSONObject();
        List<Map<String, Object>> list1 = commentService.inthreeInfor();
        List<Map<String, Object>> list = commentService.pageInthreeInfor(s,e);
        MapListToList t=new MapListToList();
        List<Infor> inforList=t.toList(list);
        JSONArray ar=JSONArray.fromObject(inforList);
        System.out.println(ar);
        Map<String, Object> result = new HashMap();
        result.put("code", 0);
        result.put("msg", "");
        result.put("count",list1.size() );
        result.put("data", ar);
        // 将其转换为JSON数据，并压入值栈返回
         j=JSONObject.fromObject(result);
         return j.toString();    
     }
}
