package com.hsd.controller;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hsd.model.RDevice;
import com.hsd.service.RDeviceService;
import com.hsd.util.JsonConversion;

@Controller
@RequestMapping("device")
public class RDeviceController {
	 @Resource  
	    private RDeviceService rDeviceService;  
    /**
     * 到设备管理页面
     */
	 @RequestMapping("/DevicesAll")    
	    public ModelAndView DevicesAll(HttpSession session){      
	        ModelAndView mav = new ModelAndView("device/aDevicesAll");   
	        return mav;    
	 }
    @RequestMapping("/DeviceExp7")    
    public ModelAndView DeviceExp7(HttpSession session){      
        ModelAndView mav = new ModelAndView("device/aDeviceExp7");   
        return mav;    
 }
    @RequestMapping("/DeviceExp15")    
    public ModelAndView DeviceExp15(HttpSession session){      
        ModelAndView mav = new ModelAndView("device/aDeviceExp15");   
        return mav;    
    }    
    @RequestMapping("/DeviceExp60")    
	 public ModelAndView DeviceExp60(HttpSession session){      
	     ModelAndView mav = new ModelAndView("device/aDeviceExp60");   
	     return mav;    
	}
	@RequestMapping("/DeviceExp")    
	    public ModelAndView DeviceExp(HttpSession session){      
	        ModelAndView mav = new ModelAndView("device/aDeviceExp");   
	   
	        return mav;    
	 }
	@RequestMapping(value="/aDeviceExpData" ,produces="application/json;charset=utf-8")  
	@ResponseBody
	public String aDeviceExpData(HttpSession session,HttpServletRequest request, 
			@RequestParam(value="page") int page,
			@RequestParam(value="limit") int limit,
    		@RequestParam(value="time") int time){ 
			String deviceName = (String) request.getParameter("deviceName");
	        int start = (page-1)*limit + 1; //分页操作 
	        int end = page*limit;
	        int count = 0;
	        List<RDevice> rdeviceList=null;
	        if(deviceName!=null&&deviceName!=""){
	        	if(time==1){
		        	rdeviceList = rDeviceService.findDeviceExp(start,end,time,deviceName);
		            count = rDeviceService.getCountAll();
		        }else if(time==0){
		        	rdeviceList = rDeviceService.findAllDeviceExp(start,end,deviceName);
			        count = rDeviceService.getCount();
		        }else{
		        	rdeviceList = rDeviceService.findDeviceExpData(start,end,time,deviceName);
			        count = rDeviceService.getCountData(time);
		        }
	        }else{
	        	if(time==1){
		        	rdeviceList = rDeviceService.aDevicesAll(start,end);
		            count = rDeviceService.getCountAll();
		        }else if(time==0){
		        	rdeviceList = rDeviceService.aDeviceExp(start,end);
			        count = rDeviceService.getCount();
		        }else{
		        	rdeviceList = rDeviceService.aDeviceExpData(start,end,time);
			        count = rDeviceService.getCountData(time);
		        }
	        }
	        //拼接成json字符串  满足layui定义的表格渲染格式
	       Map<Object, Object> map = new HashMap<Object, Object>();
	       if (rdeviceList!=null && rdeviceList.size()>0) {	
	           map.put("code", 0);
	           map.put("msg", "success");
	           map.put("count", count);
	           map.put("data", rdeviceList);
	       }else {
	           map.put("code", -1);
	           map.put("msg", "无数据");
	           map.put("data", "");
	       }
	       return  JsonConversion.writeMapJSON(map);
		}
	
}
