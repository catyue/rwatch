package com.hsd.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hsd.model.RUser;
import com.hsd.service.RUserService;
import com.hsd.util.AjaxJson;

@Controller
@RequestMapping("/login")
public class LoginController {
     
    @Autowired
    private RUserService userService;
    

   
    @RequestMapping(value = {"index", ""})
    public String index(){
        
        return "index";
    }   
    

    @RequestMapping(value ="doLogin", method = RequestMethod.POST)
    @ResponseBody
    public AjaxJson doLogin(String username, String password,HttpSession session){
        
        AjaxJson j = new AjaxJson();
        
        RUser user = userService.findUser(username,password);
       
        if (user != null) {
            j.setSuccess(true);
            session.setAttribute("user", user);
            session.setAttribute("userName", username);
        } else {
            j.setSuccess(false);
            j.setMsg("登陆成功！");
        }
        return j;
    }
        

    @RequestMapping(value = "login")
    public String login(){
        
        
        return "login";
    }
    
    @RequestMapping(value = "logout")
    public String logout(){
        
        
        return "redirect:/login/index";
    }  
}
