package com.hsd.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.fasterxml.jackson.databind.JsonNode;
import com.hsd.model.RMember;
import com.hsd.service.CommentService;
import com.hsd.service.HealthService;
import com.hsd.util.AjaxJson;
import com.hsd.util.JsonConversion;
import com.hsd.util.JsonMap;

@Controller
@RequestMapping("/health")
public class HealthController {
    @Autowired
    private  HealthService healthService;
    
    @RequestMapping(value ="list")
    public String list(){
        
        return "health/list";
        
    }
    
    @RequestMapping(value ="deviceList")
    public String deviceList(){
        return "health/deviceList";       
    }
    
    @RequestMapping(value="/healthList")
    public ModelAndView tolayuiTable(HttpSession session){      
        ModelAndView mav = new ModelAndView("health/list");   
        return mav;    
    }
    
    @ResponseBody
    @RequestMapping(value="/findListByPage")
    public String findListByPage(@RequestParam(value="page") Integer page,
            @RequestParam(value="limit") Integer limit){
        Integer start = (page-1) * limit;
        Integer end = limit;
        Integer count = healthService.getCount();
        List<Map<String, Object>> list = healthService.findListByPage(start, end);
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("code", 0);
        result.put("msg", "success");      
        result.put("count", count);
        result.put("data", list);
        String json = JsonConversion.writeMapJSON(result);
        return json;
    }
    
    @RequestMapping(value="toEdit")
    public ModelAndView toEdit(){
        ModelAndView mav = new ModelAndView("health/edit");   
        return mav;    
    }
    @ResponseBody 
    @RequestMapping(value="/updateByMemberId",produces="application/json;charset=UTF-8") 
    public AjaxJson edit(HttpSession session,
            @RequestParam(value="memberId")String memberId,
            @RequestParam(value="memberLevel")String memberLevel,
            @RequestParam(value="memberName")String memberName,
            @RequestParam(value="money")String money){
     
            AjaxJson j = new AjaxJson();
            try{
                healthService.updateByMemberId(memberId, memberName, memberLevel, money);
                j.setSuccess(true);
                j.setMsg("编辑成功！");
            }catch(Exception e){
                e.printStackTrace();
                j.setSuccess(false);
            }
            return j;
    }
    
    @RequestMapping(value="history")
    public ModelAndView toHistory(){
        ModelAndView mav = new ModelAndView("health/history");   
        return mav;    
    }
    
    @ResponseBody
    @RequestMapping(value="/bloodPressureByPage")
    public String bloodPressureByPage(@RequestParam(value="page") Integer page,
            @RequestParam(value="limit") Integer limit,
            @RequestParam(value="memberId") Integer memberId){
        Integer start = (page-1) * limit;
        Integer end = limit;
        Integer count = healthService.getCount();
        List<Map<String, Object>> list = healthService.bloodPressureByPage(start, end, memberId);
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("code", 0);
        result.put("msg", "success");      
        result.put("count", count);
        result.put("data", list);
        String json = JsonConversion.writeMapJSON(result);
        return json;
    }
    
    @ResponseBody
    @RequestMapping(value="/heartRateByPage")
    public String heartRateByPage(@RequestParam(value="page") Integer page,
            @RequestParam(value="limit") Integer limit,
            @RequestParam(value="memberId") Integer memberId){
        Integer start = (page-1) * limit;
        Integer end = limit;
        Integer count = healthService.getCount();
        List<Map<String, Object>> list = healthService.heartRateByPage(start, end, memberId);
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("code", 0);
        result.put("msg", "success");      
        result.put("count", count);
        result.put("data", list);
        String json = JsonConversion.writeMapJSON(result);
        return json;
    }
    
    @ResponseBody
    @RequestMapping(value="/stepByPage")
    public String stepByPage(@RequestParam(value="page") Integer page,
            @RequestParam(value="limit") Integer limit,
            @RequestParam(value="memberId") Integer memberId){
        Integer start = (page-1) * limit;
        Integer end = limit;
        Integer count = healthService.getCount();
        List<Map<String, Object>> list = healthService.stepByPage(start, end, memberId);
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("code", 0);
        result.put("msg", "success");      
        result.put("count", count);
        result.put("data", list);
        String json = JsonConversion.writeMapJSON(result);
        return json;
    }
    
    @ResponseBody
    @RequestMapping(value="/sleepByPage")
    public String sleepByPage(@RequestParam(value="page") Integer page,
            @RequestParam(value="limit") Integer limit,
            @RequestParam(value="memberId") Integer memberId){
        Integer start = (page-1) * limit;
        Integer end = limit;
        Integer count = healthService.getCount();
        List<Map<String, Object>> list = healthService.sleepByPage(start, end, memberId);
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("code", 0);
        result.put("msg", "success");      
        result.put("count", count);
        result.put("data", list);
        String json = JsonConversion.writeMapJSON(result);
        return json;
    }
    
    @ResponseBody
    @RequestMapping(value="/bloodSuggerByPage")
    public String bloodSuggerByPage(@RequestParam(value="page") Integer page,
            @RequestParam(value="limit") Integer limit,
            @RequestParam(value="memberId") Integer memberId){
        Integer start = (page-1) * limit;
        Integer end = limit;
        Integer count = healthService.getCount();
        List<Map<String, Object>> list = healthService.bloodSuggerByPage(start, end, memberId);
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("code", 0);
        result.put("msg", "success");      
        result.put("count", count);
        result.put("data", list);
        String json = JsonConversion.writeMapJSON(result);
        return json;
    }
}
