package com.hsd.service;

import java.util.List;

import com.hsd.model.RNotice;
import com.hsd.model.RUser;

public interface RNoticeService {

    int getCount();

    List<RUser> findListByPage(int start, int end);

    void updateflag(int noticeId);
    
    void updatenotice(String content, int noticeId, String updateBy);

    void selectbynoticeidcheckmeg(int noticeId);
    
    void sendmynotice(String addresser, String title, String content,String sender);

    List<RUser> findListBysender(int start, int end, String sender);

    List<RUser> searchUserByName(String sender);


    int getCountBysender(String sender);

    List<RUser> searchnoticebysender(int start, int end, String sender, String status);

    List<RUser> searchnoticebystatus(int start, int end, String status);

    int getCountBystatus(String status);





}
