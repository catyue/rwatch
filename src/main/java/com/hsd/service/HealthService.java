package com.hsd.service;

import java.util.List;
import java.util.Map;

import com.hsd.model.RMember;


public interface HealthService {

    Integer getCount();

    List<Map<String, Object>> findListByPage(Integer start, Integer end);

    void updateByMemberId(String memberId, String memberName, String memberLevel, String money);

    List<Map<String, Object>> bloodPressureByPage(Integer start, Integer end, Integer memberId);
    
    List<Map<String, Object>> heartRateByPage(Integer start, Integer end, Integer memberId);
    
    List<Map<String, Object>> stepByPage(Integer start, Integer end, Integer memberId);
    
    List<Map<String, Object>> sleepByPage(Integer start, Integer end, Integer memberId);
    
    List<Map<String, Object>> bloodSuggerByPage(Integer start, Integer end, Integer memberId);



}
