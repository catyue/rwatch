package com.hsd.service;

import java.util.List;

import com.hsd.model.RDevice;

public interface RDeviceService {

	List<RDevice> aDevicesAll(int start, int end);
	List<RDevice> findDeviceExp(int start, int end,int time, String deviceName);
	int getCountAll();

	List<RDevice> aDeviceExpData(int start, int end,int time);
	List<RDevice> findDeviceExpData(int start, int end, int time, String deviceName);
	int getCountData(int time);
	
	List<RDevice> aDeviceExp(int start, int end);
	List<RDevice> findAllDeviceExp(int start, int end, String deviceName);
	int getCount();

}
