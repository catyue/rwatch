package com.hsd.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hsd.mapper.HealthMapper;
import com.hsd.mapper.RDeviceMapper;
import com.hsd.mapper.RMemberMapper;
import com.hsd.mapper.RUserMapper;
import com.hsd.model.RDevice;
import com.hsd.model.RMember;
import com.hsd.model.RUser;
import com.hsd.service.HealthService;

@Service
public class HealthServiceImpl implements HealthService {
    @Autowired
    private HealthMapper healthMapper;
    @Autowired
    private RMemberMapper rMemberMapper;
    @Autowired 
    private RDeviceMapper rDeviceMapper;
    @Autowired
    private RUserMapper rUserMapper;
    @Override
    public Integer getCount() {
        return healthMapper.getCount();
    }

    @Override
    public List<Map<String, Object>> findListByPage(Integer start, Integer end) {
        return healthMapper.findListByPage(start, end);
    }


    @Override
    public void updateByMemberId(String memberId, String memberName, String memberLevel, String money) {
        healthMapper.updateByMemberId(memberId, memberName, memberLevel, money);
    }

    @Override
    public List<Map<String, Object>> bloodPressureByPage(Integer start, Integer end, Integer memberId) {
        return healthMapper.bloodPressureByPage(start, end, memberId);
    }

    @Override
    public List<Map<String, Object>> heartRateByPage(Integer start, Integer end, Integer memberId) {
        //
        return healthMapper.heartRateByPage(start, end, memberId);
    }

    @Override
    public List<Map<String, Object>> stepByPage(Integer start, Integer end, Integer memberId) {
        //
        return healthMapper.stepByPage(start, end, memberId);
    }

    @Override
    public List<Map<String, Object>> sleepByPage(Integer start, Integer end, Integer memberId) {
        //
        return healthMapper.sleepByPage(start, end, memberId);
    }

    @Override
    public List<Map<String, Object>> bloodSuggerByPage(Integer start, Integer end, Integer memberId) {
        //
        return healthMapper.bloodSuggerByPage(start, end, memberId);
    }
    
    

}
