package com.hsd.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hsd.service.RNoticeService;
import com.hsd.model.RNotice;
import com.hsd.model.RUser;
import com.hsd.mapper.RNoticeMapper;

@Service
public class RNoticeServiceImpl implements RNoticeService {
    @Autowired
    private RNoticeMapper RNoticeMapper;

    public int getCount() {
        // TODO 自动生成的方法存根
        return RNoticeMapper.getCount();
    }

    public List<RUser> findListByPage(int start, int end) {
        // TODO 自动生成的方法存根
        return RNoticeMapper.findListByPage(start,end);
    }

    public void updateflag(int noticeId) {
        // TODO 自动生成的方法存根
        RNoticeMapper.updateflag(noticeId);
        
    }

    public void updatenotice(String content, int noticeId, String updateBy) {
        // TODO 自动生成的方法存根
        RNoticeMapper.updatenotice(content, noticeId,updateBy);
    }

    public void selectbynoticeidcheckmeg(int noticeId) {
        // TODO 自动生成的方法存根
        RNoticeMapper.selectbynoticeidcheckmeg(noticeId);
        
    }

    public void sendmynotice(String addresser, String title, String content, String sender) {
        // TODO 自动生成的方法存根
        RNoticeMapper.sendmynotice(addresser, title, content,sender);
    }

    public List<RUser> findListBysender(int start, int end, String sender) {
        // TODO 自动生成的方法存根
        return RNoticeMapper.findListBysender(start,end,sender);
    }

    @Override
    public List<RUser> searchUserByName(String sender) {
        // TODO Auto-generated method stub
        return RNoticeMapper.searchUserByName(sender);
    }

    public List<RUser> searchnoticebysender(int start, int end, String sender,String status) {
        // TODO Auto-generated method stub
        return RNoticeMapper.searchnoticebysender(start,end,sender,status);
    }

    @Override
    public int getCountBysender(String sender) {
        // TODO Auto-generated method stub
        return RNoticeMapper.getCountBysender(sender);
    }

    @Override
    public List<RUser> searchnoticebystatus(int start, int end, String status) {
        // TODO Auto-generated method stub
        return RNoticeMapper.searchnoticebystatus(start,end,status);
    }

    @Override
    public int getCountBystatus(String status) {
        // TODO Auto-generated method stub
        return RNoticeMapper.getCountBystatus(status);
    }







}
