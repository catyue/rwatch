package com.hsd.service.impl;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hsd.mapper.RDeviceMapper;
import com.hsd.model.RDevice;
import com.hsd.service.RDeviceService;

@Service
public class RDeviceServiceImpl implements RDeviceService {
    
	@Autowired
    private RDeviceMapper rDeviceMapper;
	
	@Override
	public List<RDevice> aDevicesAll(int start, int end) {
		// TODO Auto-generated method stub
		return rDeviceMapper.aDevicesAll(start,end);
	}
	@Override
	public List<RDevice> findDeviceExp(int start, int end,int time, String deviceName) {
		// TODO Auto-generated method stub
		return rDeviceMapper.findDeviceExp(start,end,time,deviceName);
	}
	@Override
	public int getCountAll() {
		// TODO Auto-generated method stub
		return rDeviceMapper.getCountAll();
	}
	
	
	
	@Override
	public List<RDevice> aDeviceExpData(int start, int end,int time) {
		// TODO Auto-generated method stub
		return rDeviceMapper.aDeviceExpData(start,end,time);
	}
	@Override
	public List<RDevice> findDeviceExpData(int start, int end, int time, String deviceName) {
		// TODO Auto-generated method stub
		return rDeviceMapper.findDeviceExpData(start,end,time,deviceName);
	}
	
	@Override
	public int getCountData(int time) {
		// TODO Auto-generated method stub
		return rDeviceMapper.getCountData(time);
	}
	
	
	@Override
	public List<RDevice> aDeviceExp(int start, int end) {
		// TODO Auto-generated method stub
		return rDeviceMapper.aDeviceExp(start,end);
	}
	@Override
	public List<RDevice> findAllDeviceExp(int start, int end, String deviceName) {
		// TODO Auto-generated method stub
		return rDeviceMapper.findAllDeviceExp(start,end,deviceName);
	}
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return rDeviceMapper.getCount();
	}
	

	
	
}
