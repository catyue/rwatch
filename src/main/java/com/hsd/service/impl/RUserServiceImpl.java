package com.hsd.service.impl;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hsd.mapper.RUserMapper;
import com.hsd.model.RUser;
import com.hsd.service.RUserService;

@Service
public class RUserServiceImpl implements RUserService {
    
    @Autowired
    private RUserMapper RUserMapper;

    @Override
    public RUser findUser(String username, String password) {
        // TODO Auto-generated method stub
        return RUserMapper.findUser(username,password);
    }

    @Override
    public String selectPasswordById(int userId) {
        // TODO Auto-generated method stub
        return RUserMapper.selectPasswordById(userId);
    }

    @Override
    public void updatePasswordById(String userId, String newPassword) {
        // TODO Auto-generated method stub
        RUserMapper.updatePasswordById(userId, newPassword);
    }

    @Override
    public void updateUserById(RUser user) {
        // TODO Auto-generated method stub
    	
        RUserMapper.updateByPrimaryKey(user);
    }


      
}
