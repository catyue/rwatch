package com.hsd.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hsd.mapper.CommentMapper;
import com.hsd.mapper.RMemberMapper;
import com.hsd.mapper.RUserMapper;
import com.hsd.model.RMember;
import com.hsd.model.RUser;
import com.hsd.service.CommentService;

@Service
public class CommentServiceImpl implements CommentService {
    @Autowired
    private CommentMapper commentMapper;
    @Autowired
    private RMemberMapper rMemberMapper;
    
    //查看所有消息
    @Override
    public List<Map<String, Object>> listInfor() {
        return commentMapper.listInfor();
    }
    @Override
    public List<Map<String, Object>> listPageInfor(int s, int e) {
        return commentMapper.listPageInfor(s,e);
    }
    
    //查看离线报警信息
    @Override
    public List<Map<String, Object>> offlineInfor() {
        return commentMapper.offlineInfor();
    }
    @Override
    public List<Map<String, Object>> pageOfflineInfor(int s, int e) {
        return commentMapper.pageOfflineInfor(s,e);
    }
    
    //查看未处理信息
    @Override
    public List<Map<String, Object>> unInfor() {
        return commentMapper.unInfor();
    }
    @Override
    public List<Map<String, Object>> pageUnInfor(int s, int e) {
        return commentMapper.pageUnInfor(s,e);
    }
    
    //查看三天内信息
    @Override
    public List<Map<String, Object>> inthreeInfor() {
        return commentMapper.inthreeInfor();
    }
    @Override
    public List<Map<String, Object>> pageInthreeInfor(int s, int e) {
        return commentMapper.pageInthreeInfor(s,e);
    }

    //实时定位
    @Override
    public List<Map<String, Object>> listPosition(String deviceId) {
        return commentMapper.listPosition(deviceId);
    }

    //清除未处理警报信息
    @Override
    public Boolean deleteAlarmByIMEIAndAlarmTime(String deviceId, String alarmtime,String ontime) {
        boolean result =false;
        if(commentMapper.deleteAlarmByIMEIAndAlarmTime(deviceId,alarmtime,ontime)==1){
            result=true;
        }
        return result;
    }
    
    //查找会员by设备
    @Override
    public RMember findRMemberByDeviceId(String deviceId) {
        return rMemberMapper.findRMemberByDeviceId(deviceId);
    }
    
    //编辑会员
    @Override
    public boolean editMember(RMember r) {
        boolean result =false;
        if(rMemberMapper.updateByPrimaryKeySelective(r)==1){
            result=true;
        }
        return result;
    }

}
