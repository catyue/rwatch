package com.hsd.service;

import java.util.List;
import java.util.Map;

import com.hsd.model.RMember;
import com.hsd.model.RUser;



public interface CommentService {
    //查看所有信息
    List<Map<String, Object>> listInfor();
    List<Map<String, Object>> listPageInfor(int s, int e);
    //查看离线消息
    List<Map<String, Object>> offlineInfor();
    List<Map<String, Object>> pageOfflineInfor(int s, int e);
    //查看未处理消息
    List<Map<String, Object>> unInfor();
    List<Map<String, Object>> pageUnInfor(int s, int e);
    //查看三天内消息
    List<Map<String, Object>> inthreeInfor();
    List<Map<String, Object>> pageInthreeInfor(int s, int e);
    
    
    //实时定位
    List<Map<String, Object>> listPosition(String deviceId);
    //清除未处理消息
    Boolean deleteAlarmByIMEIAndAlarmTime(String deviceId, String alarmtime,String ontime);
    //查找会员by设备
    RMember findRMemberByDeviceId(String deviceId);
    //编辑会员
    boolean editMember(RMember r);
    


}
