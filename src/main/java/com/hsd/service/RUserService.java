package com.hsd.service;

import com.hsd.model.RUser;

public interface RUserService {

    RUser findUser(String username, String password);

    String selectPasswordById(int userId);

    void updatePasswordById(String userId, String newPassword);

    void updateUserById(RUser user);
    
}
